import requestHelper from '../helpers/request.Helper';
import responseHelper from '../helpers/response.Helper';
import excelHelper from '../helpers/excel.Helper';
import authentication from '../datalayers/authentication.data';
import FuelSurcharge from '../datalayers/FuelSurcharge.data';
import IntraterminalSettings from '../datalayers/IntraterminalSettings.data';
import LaneBalance from '../datalayers/LaneBalance.data';
import LaneExclusions from '../datalayers/LaneExclusions.data';
import LaneMarginDefaults from '../datalayers/LaneMarginDefaults.data';
import LaneMarginOverrides from '../datalayers/LaneMarginOverrides.data';
import LaneMinimumDefaults, { updatePreviousRecord } from '../datalayers/LaneMinimumDefaults.data';
import LaneMinimumOverrides from '../datalayers/LaneMinimumOverrides.data';
import SizeRestrictions from '../datalayers/SizeRestrictions.data';

module.exports = {

    authentication: (req, res, configuration) => {
        var windowsaccount = req.connection.user.replace(/SAIA\\/g, '');
        authentication.getCurrent(windowsaccount, configuration).then((result) => {
            if (result.rowsAffected == 0) {
                console.log("DENIED");
            } else {
                console.log("PASSED");
                responseHelper.sendSuccess(res, result.recordset, configuration.logger);
            }
        }, (error) => {
            responseHelper.sendFailure(res, 'authentication', error, configuration.logger);
        }).catch(error => { responseHelper.sendFailure(res, 'authenticationCatch', error, configuration.logger) });

    },

    currentDetails: (req, res, configuration) => {
        // console.log("start");
        let promises = [];
        promises = [
            SizeRestrictions.getCurrent(configuration),
            LaneMinimumDefaults.getCurrent(configuration),
            LaneMarginDefaults.getCurrent(configuration),
            IntraterminalSettings.getCurrent(configuration),
            FuelSurcharge.getCurrent(configuration)
        ];

        Promise.all(promises).then(
            values => {
                //  console.log(JSON.stringify(values));
                // console.log(values);
                // let response = {
                //     bkhaulwithinloh1: values[0].recordset[0].LOH,
                //     bkhaulminweight1: values[0].recordset[0].Min_weight,
                //     bkhaulmaxweight1: values[0].recordset[0].Max_weight,
                //     bkhaulminlength1: values[0].recordset[0].Min_length,
                //     bkhaulmaxlength1: values[0].recordset[0].Max_length,
                //     bkhaulminunits1: values[0].recordset[0].Min_units,
                //     bkhaulmaxunits1: values[0].recordset[0].Max_units,

                //     balwithinloh1: values[0].recordset[1].LOH,
                //     balminweight1: values[0].recordset[1].Min_weight,
                //     balmaxweight1: values[0].recordset[1].Max_weight,
                //     balminlength1: values[0].recordset[1].Min_length,
                //     balmaxlength1: values[0].recordset[1].Max_length,
                //     balminunits1: values[0].recordset[1].Min_units,
                //     balmaxunits1: values[0].recordset[1].Max_units,

                //     headwithinloh1: values[0].recordset[2].LOH,
                //     headminweight1: values[0].recordset[2].Min_weight,
                //     headmaxweight1: values[0].recordset[2].Max_weight,
                //     headminlength1: values[0].recordset[2].Min_length,
                //     headmaxlength1: values[0].recordset[2].Max_length,
                //     headminunits1: values[0].recordset[2].Min_units,
                //     headmaxunits1: values[0].recordset[2].Max_units,

                //     defaultminimum1: values[1].recordset[0].Minimum,
                //     defaultbackhaul1: values[2].recordset[0].Margin,
                //     defaultbalanced1: values[2].recordset[1].Margin,
                //     defaultheadhaul1: values[2].recordset[2].Margin,

                //     intraterminalmargin1: values[3].recordset[0].Intraterminal_Margin,
                //     intraterminalminimum1: values[3].recordset[0].Intraterminal_Minimum,
                //     intraterminaldiscount1: values[3].recordset[0].Intraterminal_Discount,
                //     intraterminalminweight1: values[3].recordset[0].Min_weight,
                //     intraterminalmaxweight1: values[3].recordset[0].Max_weight,
                //     intraterminalminlength1: values[3].recordset[0].Min_length,
                //     intraterminalmaxlength1: values[3].recordset[0].Max_length,
                //     baserate1: values[4].recordset[0].Percentage,
                // };
                var response = responseHelper.buildModelResponse(values);
                res.json(response);
            },
            reject => {
                responseHelper.sendFailure(res, reject);
            }
        );

    },

    updateDetails: (req, res, configuration) => {
        // console.log(req.body.defaultminimum2);
        var request = '';
        request = requestHelper.formatRequest(req);
        let promises = [];
        // console.log("Update detail service");
        promises = [
            SizeRestrictions.postUpdate(request, configuration),
            LaneMinimumDefaults.postUpdate(request, configuration),
            LaneMarginDefaults.postUpdate(request, configuration),
            IntraterminalSettings.postUpdate(request, configuration),
            FuelSurcharge.postUpdate(request, configuration)
        ];

        Promise.all(promises).then(
            values => {
                //  console.log(values);
                // console.log(JSON.stringify(values));
                // let response = {
                //     bkhaulwithinloh2: values[0].recordset[0].LOH,
                //     bkhaulminweight2: values[0].recordset[0].Min_weight,
                //     bkhaulmaxweight2: values[0].recordset[0].Max_weight,
                //     bkhaulminlength2: values[0].recordset[0].Min_length,
                //     bkhaulmaxlength2: values[0].recordset[0].Max_length,
                //     bkhaulminunits2: values[0].recordset[0].Min_units,
                //     bkhaulmaxunits2: values[0].recordset[0].Max_units,

                //     balwithinloh2: values[0].recordset[1].LOH,
                //     balminweight2: values[0].recordset[1].Min_weight,
                //     balmaxweight2: values[0].recordset[1].Max_weight,
                //     balminlength2: values[0].recordset[1].Min_length,
                //     balmaxlength2: values[0].recordset[1].Max_length,
                //     balminunits2: values[0].recordset[1].Min_units,
                //     balmaxunits2: values[0].recordset[1].Max_units,

                //     headwithinloh2: values[0].recordset[2].LOH,
                //     headminweight2: values[0].recordset[2].Min_weight,
                //     headmaxweight2: values[0].recordset[2].Max_weight,
                //     headminlength2: values[0].recordset[2].Min_length,
                //     headmaxlength2: values[0].recordset[2].Max_length,
                //     headminunits2: values[0].recordset[2].Min_units,
                //     headmaxunits2: values[0].recordset[2].Max_units,

                //     defaultminimum2: values[1].recordset[0].Minimum,
                //     defaultbackhaul2: values[2].recordset[0].Margin,
                //     defaultbalanced2: values[2].recordset[1].Margin,
                //     defaultheadhaul2: values[2].recordset[2].Margin,

                //     intraterminalmargin2: values[3].recordset[0].Intraterminal_Margin,
                //     intraterminalminimum2: values[3].recordset[0].Intraterminal_Minimum,
                //     intraterminaldiscount2: values[3].recordset[0].Intraterminal_Discount,
                //     intraterminalminweight2: values[3].recordset[0].Min_weight,
                //     intraterminalmaxweight2: values[3].recordset[0].Max_weight,
                //     intraterminalminlength2: values[3].recordset[0].Min_length,
                //     intraterminalmaxlength2: values[3].recordset[0].Max_length,
                //     baserate2: values[4].recordset[0].Percentage,
                // };
                
                var response = responseHelper.buildModelResponse(values);
                console.log(response);
                res.json(response);
            },
            reject => {
                responseHelper.sendFailure(res, reject);
            }
        );
    },


    currentLaneBalances: (req, res, configuration) => {
        LaneBalance.getCurrent(configuration).then( (result) => {

            let filename = 'Lane Balance Data.xlsx';
            // filename += 'Points by Terminals.xlsx';
            // console.log(result);
            responseHelper.sendWorkbook(
              res,
              excelHelper.generateLaneBalanceExcelSheet(result),
              filename
            );

            // responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, (error) => {
            responseHelper.sendFailure(res, 'currentLaneBalances', error, configuration.logger);
        }).catch(error => { responseHelper.sendFailure(res, 'currentLaneBalancesCatch', error, configuration.logger) });
    },

    updateLaneBalances: (req, res, configuration) => {
        LaneBalance.postUpdate(configuration).then((result) => {
            responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, (error) => {
            responseHelper.sendFailure(res, 'updateLaneBalances', error, configuration.logger);
        }).catch(error => { responseHelper.sendFailure(res, 'updateLaneBalancesCatch', error, configuration.logger) });
    },


    currentLaneExclusions: (req, res, configuration) => {
        LaneExclusions.getCurrent(configuration).then((result) => {
            let filename = 'Lane Exclusion Data.xlsx';
            responseHelper.sendWorkbook(
              res,
              excelHelper.generateLaneExclusionExcelSheet(result),
              filename
            );
            // responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, (error) => {
            responseHelper.sendFailure(res, 'currentLaneExclusions', error, configuration.logger);
        }).catch(error => { responseHelper.sendFailure(res, 'currentLaneExclusionsCatch', error, configuration.logger) });
    },

    updateLaneExclusions: (req, res, configuration) => {
        LaneExclusions.postUpdate(req, configuration).then((result) => {
            responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, (error) => {
            responseHelper.sendFailure(res, 'updateLaneExclusions', error, configuration.logger);
        }).catch(error => { responseHelper.sendFailure(res, 'updateLaneExclusionsCatch', error, configuration.logger) });
    },


    currentMinimumCharge: (req, res, configuration) => {
        LaneMinimumOverrides.getCurrent(configuration).then((result) => {
            let filename = 'Minimum Charge Data.xlsx';
            // filename += 'Points by Terminals.xlsx';
            // console.log(result);
            responseHelper.sendWorkbook(
              res,
              excelHelper.generateMinimumExcelSheet(result),
              filename
            );
            // responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, (error) => {
            responseHelper.sendFailure(res, 'currentMinimumCharge', error, configuration.logger);
        }).catch(error => { responseHelper.sendFailure(res, 'currentMinimumChargeCatch', error, configuration.logger) });
    },

    updateMinimumCharge: (req, res, configuration) => {
        // var windowsaccount = req.connection.user.replace(/SAIA\\/g,'');
        LaneMinimumOverrides.postUpdate(configuration)
            .then((result) => {
                responseHelper.sendSuccess(res, result.recordset, configuration.logger);
            }, (error) => {
                responseHelper.sendFailure(res, 'servicesGetAll', error, configuration.logger);
            }).catch(error => { responseHelper.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger) });

    },


    currentProfit: (req, res, configuration) => {
        LaneMarginOverrides.getCurrent(configuration).then((result) => {
            let filename = 'Profit Margin Data.xlsx';
            responseHelper.sendWorkbook(
              res,
              excelHelper.generateProfitSheet(result),
              filename
            );
            // responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, (error) => {
            responseHelper.sendFailure(res, 'currentProfit', error, configuration.logger);
        }).catch(error => { responseHelper.sendFailure(res, 'currentProfitCatch', error, configuration.logger) });
    },

    updateUpdate: (req, res, configuration) => {
        LaneMarginOverrides.postUpdate(configuration).then((result) => {
            responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, (error) => {
            responseHelper.sendFailure(res, 'updateUpdate', error, configuration.logger);
        }).catch(error => { responseHelper.sendFailure(res, 'updateUpdateCatch', error, configuration.logger) });
    }

}