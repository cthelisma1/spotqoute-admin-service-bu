let sql = require('mssql');
let xlsx = require('xlsx');

// all sql statements
const selectAllCurrent = 'SELECT TOP (1) OriginTerminal, DestinationTerminal, Minimum FROM PRICING_APP.dbo.LaneMinimumOverrides WHERE EndDate IS NULL ORDER BY OriginTerminal, DestinationTerminal';
const closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneMinimumOverrides SET EndDate = GETDATE() WHERE EndDate IS NULL and OriginTerminal = @OriginTerminal and DestinationTerminal = @DestinationTerminal';
const newRecord = 'INSERT INTO PRICING_APP.dbo.LaneMinimumOverrides(OriginTerminal,DestinationTerminal,Minimum,StartDate,EndDate,Lane,ChangedBy) VALUES(@OriginTerminal,@DestinationTerminal,@Minimum,GETDATE(),NULL,@Lane,@ChangedBy)';

const LaneMinimumOverridesFields = {
  OriginTerminal: { name: 'OriginTerminal', dtype: sql.VarChar, length: 100 },
  DestinationTerminal: { name: 'DestinationTerminal', dtype: sql.VarChar, length: 4000 },
  Minimum: { name: 'Minimum', dtype: sql.int, length: 1000 },
  StartDate: { name: 'StartDate', dtype: sql.DateTime, length: 100 },
  EndDate: { name: 'EndDate', dtype: sql.DateTime, length: 100 },
  Lane: { name: 'Lane', dtype: sql.VarChar, length: 100 },
  ChangedBy: { name: 'ChangedBy', dtype: sql.VarChar, length: 4000 }
};

module.exports = {

  // list of table fields.
  LaneMinimumOverridesList: LaneMinimumOverridesFields,

  getCurrent: (configuration) => {
    let pool = configuration.db.pool;
    var request = new sql.Request(pool);

    // pull all current records from database and return.
    return request
      .query(selectAllCurrent);
  },

  postUpdate: (configuration) => {
    let pool = configuration.db.pool;
    var request = new sql.Request(pool);
    var currentRecord;
    var fileMin;
    var currentDatabaseData = [];

    const workbook = xlsx.readFile('Minimum Charge Data.xlsx');
    const snl = workbook.SheetNames;
    // convert Minimum Charge Data.xlsx sheet to JSON - contains new values that should update LaneMinimumOverrides.
    var newFileUpload = JSON.parse(JSON.stringify(xlsx.utils.sheet_to_json(workbook.Sheets[snl[0]])));

    // query LaneMinimumOverrides
    request.query(selectAllCurrent, function (err, rows) {
      if (err) {
        throw err;
      } else {
        // Process data 
        setValue(rows);
      }
    });

    // get current values in LaneMinimumOverrides and convert into JSON - and Update table accordingly. 
    function setValue(value) {
      currentDatabaseData = JSON.parse(JSON.stringify(value));
      for (let i = 0; i < newFileUpload.length; i++) {
        fileMin = newFileUpload[i].Minimum;

        // find the correspondingly lane and return
        currentRecord = currentDatabaseData.recordset.filter(function (value) {
          return value.OriginTerminal == newFileUpload[i].OriginTerminal && value.DestinationTerminal == newFileUpload[i].DestinationTerminal;
        });

        // close out previous record by setting the EndDate with date.
        if (currentRecord[0].Minimum != newFileUpload[i].Minimum && currentRecord[0].length !== 0) {
          console.log("Current Minimum: " + currentRecord.Minimum + "New Minimum: " + newFileUpload[i].Minimum);
          if (currentRecord.length !== 0) {
            request
              .input(LaneMinimumOverridesFields.OriginTerminal.name, LaneMinimumOverridesFields.OriginTerminal.dtype, newFileUpload[i].OriginTerminal)
              .input(LaneMinimumOverridesFields.DestinationTerminal.name, LaneMinimumOverridesFields.DestinationTerminal.dtype, newFileUpload[i].DestinationTerminal)
              .query(closePrevRecord, function (err) {
                if (err) {
                  throw err;
                }
              });
            console.log("Closed Record: OT=" + newFileUpload[i].OriginTerminal + " DT=" + newFileUpload[i].DestinationTerminal + " Old Minimum:" + currentRecord[i].Minimum);
          }

          // insert new record with proper values and a EndDate = null
          var lane = newFileUpload[i].OriginTerminal + "-" + newFileUpload[i].DestinationTerminal;
          request
            .input(LaneMinimumOverridesFields.OriginTerminal.name, LaneMinimumOverridesFields.OriginTerminal.dtype, newFileUpload[i].OriginTerminal)
            .input(LaneMinimumOverridesFields.DestinationTerminal.name, LaneMinimumOverridesFields.DestinationTerminal.dtype, newFileUpload[i].DestinationTerminal)
            .input(LaneMinimumOverridesFields.Minimum.name, LaneMinimumOverridesFields.Minimum.dtype, newFileUpload[i].Minimum)
            .input(LaneMinimumOverridesFields.Lane.name, LaneMinimumOverridesFields.Lane.dtype, lane)
            .input(LaneMinimumOverridesFields.ChangedBy.name, LaneMinimumOverridesFields.ChangedBy.dtype, "chris")
            .query(newRecord, function (err) {
              if (err) {
                throw err;
              } else {
                console.log("New Record: OT=" + newFileUpload[i].OriginTerminal + " DT=" + newFileUpload[i].DestinationTerminal + " New Minimum:" + newFileUpload[i].Minimum + " Lane: " + lane);
              }
            });
          // }
        }
      }
    }
    return request.query(selectAllCurrent);
  }
};