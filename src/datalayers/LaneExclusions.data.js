let sql = require('mssql');
let xlsx = require('xlsx');

// All sql statements
const selectAllCurrent = 'SELECT TOP (1) OriginTerminal, DestinationTerminal, LoadPlan, Excluded, Reason FROM PRICING_APP.dbo.LaneExclusions WHERE EndDate IS NULL ORDER BY OriginTerminal, DestinationTerminal';
const closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneExclusions SET EndDate = GETDATE() WHERE Lane = @Lane and EndDate IS NULL';
const newRecord = 'INSERT INTO PRICING_APP.dbo.LaneExclusions(OriginTerminal,DestinationTerminal,LoadPlan,Excluded,Reason,StartDate,EndDate,Lane,ChangedBy) VALUES(@OriginTerminal,@DestinationTerminal,@LoadPlan,@Excluded,@Reason,GETDATE(),NULL,@Lane,@ChangedBy)';

const LaneExclusionsFields = {
  OriginTerminal: { name: 'OriginTerminal', dtype: sql.NVarChar, length: 100 },
  DestinationTerminal: { name: 'DestinationTerminal', dtype: sql.NVarChar, length: 4000 },
  LoadPlan: { name: 'LoadPlan', dtype: sql.VarChar, length: 1000 },
  Excluded: { name: 'Excluded', dtype: sql.Int, length: 1000 },
  Reason: { name: 'Reason', dtype: sql.Text, length: 1000 },
  StartDate: { name: 'StartDate', dtype: sql.DateTime, length: 100 },
  EndDate: { name: 'EndDate', dtype: sql.DateTime, length: 100 },
  Lane: { name: 'Lane', dtype: sql.VarChar, length: 100 },
  ChangedBy: { name: 'ChangedBy', dtype: sql.VarChar, length: 4000 }
};

module.exports = {

  // List of update table fields in the table.
  LaneExclusionsFieldsList: LaneExclusionsFields,

  getCurrent: (configuration) => {
    let pool = configuration.db.pool;
    var request = new sql.Request(pool);

    // Pull all services in database.
    return request
      .query(selectAllCurrent);
  },

  postUpdate: (req, configuration) => {
    console.log("start");
    let pool = configuration.db.pool;
    var request = new sql.Request(pool);
    var currentRecord;
    var fileMin;
    var currentDatabaseData = [];
    // const workbook = xlsx.readFile('Lane Exclusion Data.xlsx');
    const workbook = xlsx.readFile(req);
    const snl = workbook.SheetNames;

    // convert Lane Exclusion Data.xlsx sheet to JSON - contains new values that should update LaneExclusions.
    var newFileUpload = JSON.parse(JSON.stringify(xlsx.utils.sheet_to_json(workbook.Sheets[snl[0]])));
    console.log(newFileUpload);
    // query LaneExclusions
    request.query(selectAllCurrent, function (err, rows) {
      if (err) {
        throw err;
      } else {
        // Process data 
        setValue(rows);
      }
    });

    // get current values in LaneExclusions and convert into JSON - and Update table accordingly. 
    function setValue(value) {
      currentDatabaseData = JSON.parse(JSON.stringify(value));
      for (let i = 0; i < newFileUpload.length; i++) {

        // find the correspondingly lane and return
        currentRecord = currentDatabaseData.recordset.filter(function (value) {
          return value.OriginTerminal == newFileUpload[i].OriginTerminal && value.DestinationTerminal == newFileUpload[i].DestinationTerminal;
        });

        // close out previous record by setting the EndDate with date.
        if ((currentRecord[0].Excluded != newFileUpload[i].Excluded || currentRecord[0].Reason != newFileUpload[i].Reason) && currentRecord[0].length !== 0) {
          console.log("Current Excluded/Reason: " + currentRecord.Excluded + "/" + currentRecord.Reason + "New Excluded/Reason: " + newFileUpload[i].Excluded + "/" + newFileUpload[i].Reason);
          if (currentRecord.length !== 0) {
            // request
            //   .input(LaneExclusionsFields.OriginTerminal.name, LaneExclusionsFields.OriginTerminal.dtype, newFileUpload[i].OriginTerminal)
            //   .input(LaneExclusionsFields.DestinationTerminal.name, LaneExclusionsFields.DestinationTerminal.dtype, newFileUpload[i].DestinationTerminal)
            //   .query(closePrevRecord, function (err) {
              //   if (err) {
              //     throw err;
              //   }
              // });
              console.log("Closed Record: OT=" + newFileUpload[i].OriginTerminal + " DT=" + newFileUpload[i].DestinationTerminal + " Old Excluded/Reason:" + newFileUpload[i].Excluded + "/" + newFileUpload[i].Reason);
          }

          // insert new record with proper values and a EndDate = null
          var lane = newFileUpload[i].OriginTerminal + "-" + newFileUpload[i].DestinationTerminal;
          // request
          //   .input(LaneExclusionsFields.OriginTerminal.name, LaneExclusionsFields.OriginTerminal.dtype, newFileUpload[i].OriginTerminal)
          //   .input(LaneExclusionsFields.DestinationTerminal.name, LaneExclusionsFields.DestinationTerminal.dtype, newFileUpload[i].DestinationTerminal)
          //   .input(LaneExclusionsFields.LoadPlan.name, LaneExclusionsFields.LoadPlan.dtype, newFileUpload[i].LoadPlan)
          //   .input(LaneExclusionsFields.Excluded.name, LaneExclusionsFields.Excluded.dtype, newFileUpload[i].Excluded)
          //   .input(LaneExclusionsFields.Reason.name, LaneExclusionsFields.Reason.dtype, newFileUpload[i].Reason)
          //   .input(LaneExclusionsFields.Lane.name, LaneExclusionsFields.Lane.dtype, lane)
          //   .input(LaneExclusionsFields.ChangedBy.name, LaneExclusionsFields.ChangedBy.dtype, "chris")
          //   .query(newRecord, function (err) {
          //     if (err) {
          //       throw err;
          //     } else {
          //       console.log("New Record: OT=" + newFileUpload[i].OriginTerminal + " DT=" + newFileUpload[i].DestinationTerminal + " New Excluded/Reason: " + newFileUpload[i].Excluded + "/" + newFileUpload[i].Reason + " Lane: " + lane);
          //     }
          //   });
          // }
        }
      }
    }
    return request.query(selectAllCurrent);
  }
};