import responsehelper from '../helpers/response.Helper';
import IntraterminalSettings from '../datalayers/IntraterminalSettings.data';
import LaneBalance from '../datalayers/LaneBalance.data';
import LaneExclusions from '../datalayers/LaneExclusions.data';
import LaneMarginDefaults from '../datalayers/LaneMarginDefaults.data';
import LaneMarginOverrides from '../datalayers/LaneMarginOverrides.data';
import LaneMinimumDefaults from '../datalayers/LaneMinimumDefaults.data';
import LaneMinimumOverrides from '../datalayers/LaneMinimumOverrides.data';
import LaneMinimums from '../datalayers/LaneMinimums.data';
import LaneSizeMaximums from '../datalayers/LaneSizeMaximums.data';
import LaneSizeMinimums from '../datalayers/LaneSizeMinimums.data';
import FuelSurcharge from '../datalayers/FuelSurcharge.data';
module.exports = {

currentDetails: (req, res, configuration) => {
    LaneMinimumDefaults.getCurrent(configuration).then((result) => {
        responsehelper.sendSuccess(res, result.recordset, configuration.logger);

        LaneMinimumDefaults.getCurrent(configuration).then((result) => {
            responsehelper.sendSuccess(res, result.recordset, configuration.logger);

            LaneMinimumDefaults.getCurrent(configuration).then((result) => {
                responsehelper.sendSuccess(res, result.recordset, configuration.logger);

                LaneMinimumDefaults.getCurrent(configuration).then((result) => {
                    responsehelper.sendSuccess(res, result.recordset, configuration.logger);

                    LaneMinimumDefaults.getCurrent(configuration).then((result) => {
                        responsehelper.sendSuccess(res, result.recordset, configuration.logger);

                        LaneMinimumDefaults.getCurrent(configuration).then((result) => {
                            responsehelper.sendSuccess(res, result.recordset, configuration.logger);
                        }, (error) => {
                            responsehelper.sendFailure(res, 'servicesGetAll', error, configuration.logger);
                        }).catch( error => { responsehelper.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger) });
                    
                    }, (error) => {
                        responsehelper.sendFailure(res, 'servicesGetAll', error, configuration.logger);
                    }).catch( error => { responsehelper.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger) });


                }, (error) => {
                    responsehelper.sendFailure(res, 'servicesGetAll', error, configuration.logger);
                }).catch( error => { responsehelper.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger) });


            }, (error) => {
                responsehelper.sendFailure(res, 'servicesGetAll', error, configuration.logger);
            }).catch( error => { responsehelper.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger) });

        }, (error) => {
            responsehelper.sendFailure(res, 'servicesGetAll', error, configuration.logger);
        }).catch( error => { responsehelper.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger) });

    }, (error) => {
        responsehelper.sendFailure(res, 'servicesGetAll', error, configuration.logger);
    }).catch( error => { responsehelper.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger) });
}
}