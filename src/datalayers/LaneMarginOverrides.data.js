let sql = require('mssql');
let xlsx = require('xlsx');

// All sql statements
const selectAllCurrent = 'SELECT TOP (1) OriginTerminal, DestinationTerminal, Lane, Margin FROM PRICING_APP.dbo.LaneMarginOverrides WHERE EndDate IS NULL ORDER BY Lane';
const closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneMarginOverrides SET EndDate = GETDATE() WHERE OriginTerminal = @OriginTerminal and DestinationTerminal = @DestinationTerminal and EndDate IS NULL';
const newRecord = 'INSERT INTO PRICING_APP.dbo.LaneMarginOverrides(OriginTerminal,DestinationTerminal,LoadPlan,StartDate,EndDate,Lane,ChangedBy) VALUES(@OriginTerminal,@DestinationTerminal,@LoadPlan,GETDATE(),NULL,@Lane,@ChangedBy)';

const LaneMarginOverridesFields = {
  OriginTerminal: { name: 'OriginTerminal', dtype: sql.NVarChar, length: 100 },
  DestinationTerminal: { name: 'DestinationTerminal', dtype: sql.NVarChar, length: 4000 },
  LoadPlan: { name: 'LoadPlan', dtype: sql.NVarChar, length: 1000 },
  Margin: { name: 'Margin', dtype: sql.Int, length: 1000 },
  StartDate: { name: 'StartDate', dtype: sql.DateTime, length: 100 },
  EndDate: { name: 'EndDate', dtype: sql.DateTime, length: 100 },
  Lane: { name: 'Lane', dtype: sql.VarChar, length: 100 },
  ChangedBy: { name: 'ChangedBy', dtype: sql.VarChar, length: 4000 }
};

module.exports = {

  // List of update table fields in the table.
  LaneMarginOverridesFieldsList: LaneMarginOverridesFields,

  getCurrent: (configuration) => {
    let pool = configuration.db.pool;
    var request = new sql.Request(pool);

    // Pull all services in database.
    return request
      .query(selectAllCurrent);
  },

  postUpdate: (configuration) => {
    let pool = configuration.db.pool;
    var request = new sql.Request(pool);
    var currentRecord;
    var fileMin;
    var currentDatabaseData = [];
    const workbook = xlsx.readFile('Profit Margin Data.xlsx');
    const snl = workbook.SheetNames;

    // convert Profit Margin Data.xlsx sheet to JSON - contains new values that should update LaneMarginOverrides.
    var newFileUpload = JSON.parse(JSON.stringify(xlsx.utils.sheet_to_json(workbook.Sheets[snl[0]])));

    // query LaneMarginOverrides
    request.query(selectAllCurrent, function (err, rows) {
      if (err) {
        throw err;
      } else {
        // Process data 
        setValue(rows);
      }
    });

    // get current values in LaneMarginOverrides and convert into JSON - and Update table accordingly. 
    function setValue(value) {
      currentDatabaseData = JSON.parse(JSON.stringify(value));
      for (let i = 0; i < newFileUpload.length; i++) {

        // find the correspondingly lane and return
        currentRecord = currentDatabaseData.recordset.filter(function (value) {
          return value.OriginTerminal == newFileUpload[i].OriginTerminal && value.DestinationTerminal == newFileUpload[i].DestinationTerminal;
        });

        // close out previous record by setting the EndDate with date.
        if (currentRecord[0].Margin != newFileUpload[i].Margin && currentRecord[0].length !== 0) {
          console.log("Current Margin: " + currentRecord.Margin + "New Margin: " + newFileUpload[i].Margin);
          if (currentRecord.length !== 0) {
            request
              .input(LaneMarginOverridesFields.OriginTerminal.name, LaneMarginOverridesFields.OriginTerminal.dtype, newFileUpload[i].OriginTerminal)
              .input(LaneMarginOverridesFields.DestinationTerminal.name, LaneMarginOverridesFields.DestinationTerminal.dtype, newFileUpload[i].DestinationTerminal)
              .query(closePrevRecord, function (err) {
                if (err) {
                  throw err;
                }
              });
            console.log("Closed Record: OT=" + newFileUpload[i].OriginTerminal + " DT=" + newFileUpload[i].DestinationTerminal + " Old Margin:" + currentRecord[i].Margin);
          }

          // insert new record with proper values and a EndDate = null
          var lane = newFileUpload[i].OriginTerminal + "-" + newFileUpload[i].DestinationTerminal;
          request
            .input(LaneMarginOverridesFields.OriginTerminal.name, LaneMarginOverridesFields.OriginTerminal.dtype, newFileUpload[i].OriginTerminal)
            .input(LaneMarginOverridesFields.DestinationTerminal.name, LaneMarginOverridesFields.DestinationTerminal.dtype, newFileUpload[i].DestinationTerminal)
            .input(LaneMarginOverridesFields.LoadPlan.name, LaneMarginOverridesFields.LoadPlan.dtype, currentRecord[i].LoadPlan)
            .input(LaneMarginOverridesFields.Lane.name, LaneMarginOverridesFields.Lane.dtype, lane)
            .input(LaneMarginOverridesFields.ChangedBy.name, LaneMarginOverridesFields.ChangedBy.dtype, "chris")
            .query(newRecord, function (err) {
              if (err) {
                throw err;
              } else {
                console.log("New Record: OT=" + newFileUpload[i].OriginTerminal + " DT=" + newFileUpload[i].DestinationTerminal + " New Margin:" + newFileUpload[i].Margin);
              }
            });
          // }
        }
      }
    }
    return request.query(selectAllCurrent);
  }
};