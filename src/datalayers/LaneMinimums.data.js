let sql = require('mssql');

// All sql statements
const selectAll = 'SELECT * FROM PRICING_APP.dbo.LaneMinimums';
const selectCurrent = 'SELECT * FROM PRICING_APP.dbo.LaneMinimums WHERE EndDate IS NULL';
const newRecord = 'INSERT INTO PRICING_APP.dbo.LaneMinimums(OriginTerminal,DestinationTerminal,LoadPlan,LOH,MinimumCharge,StartDate,EndDate,Lane,ChangedBy) VALUES(@OriginTerminal,@DestinationTerminal,@LoadPlan,@LOH,@MinimumCharge,GETDATE(),NULL,@Lane,@ChangedBy)';
const closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneMinimums SET EndDate = GETDATE() WHERE EndDate IS NULL';
const LaneMinimumsFields = {
    OriginTerminal:      { name: 'OriginTerminal', dtype: sql.NChar, length: 100 },
    DestinationTerminal: { name: 'DestinationTerminal', dtype: sql.NChar, length: 4000 },
    LoadPlan:            { name: 'LoadPlan', dtype: sql.NChar, length: 1000 },    
    LOH:                 { name: 'LOH', dtype: sql.NChar, length: 100 },
    MinimumCharge:       { name: 'MinimumCharge', dtype: sql.NChar, length: 100 },
    StartDate:           { name: 'StartDate', dtype: sql.NChar, length: 100 },
    EndDate:             { name: 'EndDate', dtype: sql.NChar, length: 100 },
    Lane:                { name: 'Lane', dtype: sql.NChar, length: 100 },
    ChangedBy:           { name: 'ChangedBy', dtype: sql.NChar, length: 4000 }
};

module.exports = {

    // List of update table fields in the table.
    LaneMinimumsFieldsList: LaneMinimumsFields,
    
    getCurrent: (configuration) => {
        let pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request
        .query(selectCurrent);
    },

    postUpdate: (configuration) => {
        let pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request
        .query(closePrevRecord);
    }

};