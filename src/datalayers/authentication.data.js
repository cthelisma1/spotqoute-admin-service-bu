let sql = require('mssql');

// All sql statements
const check = 'SELECT * FROM PRICING_APP.dbo.AdminPanelUser where username = @username';
const AdminPanelUserFields = {
    username: { name: 'username', dtype: sql.VarChar, length: 100 }
};

module.exports = {

    AdminPanelUserFieldList: AdminPanelUserFields,

    getCurrent: (req, configuration) => {
        let pool = configuration.db.pool;
        var request = new sql.Request(pool);
        return request
        .input(AdminPanelUserFields.username.name, AdminPanelUserFields.username.dtype, req)
        .query(check);

    }

};