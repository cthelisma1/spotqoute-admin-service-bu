let sql = require('mssql');

// All sql statements
const selectCurrent = 'SELECT * FROM PRICING_APP.dbo.FuelSurcharge WHERE EndDate IS NULL and Type = @Type';
const closePrevRecord = 'UPDATE PRICING_APP.dbo.FuelSurcharge SET EndDate = GETDATE() WHERE EndDate IS NULL and Type = @Type';
const newRecord = 'INSERT INTO PRICING_APP.dbo.FuelSurcharge(Percentage,Type,StartDate,EndDate,ChangedBy) VALUES(@Percentage,@Type,GETDATE(),NULL,@ChangedBy)';
const FuelSurchargeFields = {
    Percentage: { name: 'Percentage', dtype: sql.NChar, length: 100 },
    Type: { name: 'Type', dtype: sql.NChar, length: 4000 },
    StartDate: { name: 'StartDate', dtype: sql.NChar, length: 100 },
    EndDate: { name: 'EndDate', dtype: sql.NChar, length: 100 },
    ChangedBy: { name: 'ChangedBy', dtype: sql.NChar, length: 4000 }
};

module.exports = {

    // List of update table fields in the table.
    FuelSurchargeFieldsList: FuelSurchargeFields,

    getCurrent: (configuration) => {
        let pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request
            .input(FuelSurchargeFields.Type.name, FuelSurchargeFields.Type.dtype, "base")
            .query(selectCurrent);
    },

    postUpdate: (req, configuration) => {
        let pool = configuration.db.pool;
        var request = new sql.Request(pool);
        console.log("Fuel Surcharge");
        // Pull all services in database.
        if (req.body.fuelsurcharge[0].update == 1) {
            request
                .input(FuelSurchargeFields.Type.name, FuelSurchargeFields.Type.dtype, req.body.fuelsurcharge[0].type)
                .query(closePrevRecord);

            request
                .input(FuelSurchargeFields.Percentage.name, FuelSurchargeFields.Percentage.dtype, req.body.fuelsurcharge[0].percentage)
                .input(FuelSurchargeFields.Type.name, FuelSurchargeFields.Type.dtype, req.body.fuelsurcharge[0].type)
                .input(FuelSurchargeFields.ChangedBy.name, FuelSurchargeFields.ChangedBy.dtype, "chris")
                .query(newRecord);
                console.log("Fuel Surcharge: " + req.body.fuelsurcharge[0].percentage);
        }

        console.log("---------------------------------------------------------");
        return request
            .input(FuelSurchargeFields.Type.name, FuelSurchargeFields.Type.dtype, req.body.fuelsurcharge[0].type)
            .query(selectCurrent);
    }

};