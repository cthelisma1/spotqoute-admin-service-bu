// Setup Http end point for service DB request.

// Imports needed for Main module.
import express from 'express';
import http from 'http';
import https from 'https';
import fs from 'fs';
import winston from './config/winston';
import morgan from 'morgan';
import config from './config.json';
import bodyParser from 'body-parser';
import validator from 'express-validator';
import swagger from 'swagger-ui-express';
import swaggerConfig from './config/swagger.json';
import initAS400 from './config/as400';
import initSQL from './config/mssql';
import routesv1 from './routesv1';
import cors from 'cors';
import responses from './helpers/response.Helper';
// import asData from './datalayers/squoteAS400.data';
import { SSL_OP_NO_TLSv1 } from 'constants';

// Setup server level main objects
let app = express();

let logger = winston;

app.use(cors());

// Setup system wide logging.
app.use(morgan("combined", { "stream": winston.stream }));

// Setup http body parsing of data.
app.use(bodyParser.json({ limit: config.bodyLimit }));

// Setup url encoding/decoding
app.use(bodyParser.urlencoded({ extended: true }));

// Create instance/initialize validator object to use in application.
app.use(validator());

// Initialize the documentation infrastructure
app.use('/api-docs', swagger.serve, swagger.setup(swaggerConfig));

// Redirect all traffic to HTTPS secure protocol.
// app.use(function(req, res, next) {
//     if (req.secure) {
//         next();

//     } else {
//         res.redirect(`https://${req.host}:${require('./config.json').sslport}${req.url}`);
//     }
// });

// Authenticate the user knows the standard authentication key and passed it.
// app.use((req, res, next) => {
//     var nodeSSPI = require('node-sspi')
//     var nodeSSPIObj = new nodeSSPI({
//       retrieveGroups: true
//     })
//     nodeSSPIObj.authenticate(req, res, function(err){
//       res.finished || next()
//     })
//   });

// Open AS400 database connection pool.
initAS400(db => {

        // Open MSSQL database connection pool.
        initSQL(msdb => {
            // MSSQL connection failed. Report error abort start.
            if (msdb instanceof Error) {
                console.log(msdb);
                logger.log('error', msdb.message);
            } else {
        
                try
                {
                    // Setup global routes - For release version 1.
                    app.use('/v1', routesv1({db, msdb, logger}));

                    // Display web service documentation if browsing to web site root.
                    app.get('/', (req, res) => {
                        res.redirect('/api-docs');
                    });

                    // catch 404
                    app.all('*', function(req, res) {
                        // console.log("1");
                        responses.sendFailure(res, "server_init", "Route not found", winston);
                    });

                        try 
                        {
                            // START THE SERVER
                            // =============================================================================
                            app.httpserver = http.createServer(app);
                    
                            app.httpserver.listen(process.env.port || config.port, config.serveraddress,  () => {
                                    console.log(`HTTP Server started at ${app.httpserver.address().address} listing on port ${app.httpserver.address().port}`);
                            });
                    
                            const ssloptions = {
                                pfx: fs.readFileSync('./src/config/starsaia2019.pfx'),
                                passphrase: "Intimg1".trim(),
                                secureOptions: require('constants').SSL_OP_NO_TLSv1
                            };
                    
                             app.httpsserver = https.createServer(ssloptions, app);
                    
                            app.httpsserver.listen(process.env.port || config.sslport, config.serveraddress, () => {
                                    console.log(`HTTPS Server started at ${app.httpsserver.address().address} listing on port ${app.httpsserver.address().port}`);
                            });
                        }
                        catch (err) {
                            let msg = `Server failed to start at HTTP server initialization ${err}`
                            console.log(msg);
                            logger.log({level: 'error', message: msg });
                    }  
                }
                catch (err) {
                    let msg = `Server failed to start at route loading and swagger documentation load with error ${err}`
                    console.log(msg);
                    logger.log('error', msg);
                }
            }
        });
    
});

export default app;