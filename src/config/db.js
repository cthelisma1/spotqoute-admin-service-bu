import config from '../config.json';

let sql = require('mssql');

module.exports = {

    init: callback => {
        sql.connect(config.dbConfig.sqlConfig).then( (pool) => {
            callback({pool})
        },  (err) => {
            callback(err);
        });
    }
}