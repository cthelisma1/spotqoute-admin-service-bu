import config from '../config.json';
import ibmdb from 'ibm_db';

export default callback => {
  try {
    ibmdb.open(config.dbConfig.connectionString, (err, pool) => {
      if (err) {
        callback(err);
      } else {
        callback(pool);
      }
    });
  }
  catch (err) {
    callback(err);
  }
};
