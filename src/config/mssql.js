import config from '../config.json';
import mssql from 'mssql';

export default callback => {
  try {
    mssql
      .connect(config.dbConfig.sqlConfig)
      .then(pool => {
        callback(pool);
      })
      .catch(err => {
        callback(err);
      });
  } catch (err) {
    callback(err);
  }
};