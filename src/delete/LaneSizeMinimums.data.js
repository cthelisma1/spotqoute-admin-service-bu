let sql = require('mssql');

// All sql statements
const selectCurrent = 'SELECT * FROM PRICING_APP.dbo.LaneSizeMinimums WHERE EndDate IS NULL';
const newRecord = 'INSERT INTO PRICING_APP.dbo.LaneSizeMinimums(OriginTerminal,DestinationTerminal,LoadPlan,MinimumWeight,MinimumLength,MinimumUnits,StartDate,EndDate,ChangedBy) VALUES(@OriginTerminal,@DestinationTerminal,@LoadPlan,@Minimum,@MinimumLength,@MinimumUnits,GETDATE(),NULL,@ChangedBy)';
const closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneSizeMinimums SET EndDate = GETDATE() WHERE EndDate IS NULL';
const LaneSizeMinimumsFields = {
    OriginTerminal:      { name: 'OriginTerminal', dtype: sql.NChar, length: 4000 },
    DestinationTerminal: { name: 'DestinationTerminal', dtype: sql.NChar, length: 4000 },
    LoadPlan:            { name: 'LoadPlan', dtype: sql.NChar, length: 4000 },
    MinimumWeight: { name: 'MinimumWeight', dtype: sql.NChar, length: 100 },
    MinimumLength: { name: 'MinimumLength', dtype: sql.NChar, length: 4000 },
    MinimumUnits:  { name: 'MinimumUnits', dtype: sql.NChar, length: 1000 },    
    StartDate:     { name: 'StartDate', dtype: sql.NChar, length: 100 },
    EndDate:       { name: 'EndDate', dtype: sql.NChar, length: 100 },
    ChangedBy:     { name: 'ChangedBy', dtype: sql.NChar, length: 4000 }
};

module.exports = {

    // List of update table fields in the table.
    LaneSizeMinimumsFieldsList: LaneSizeMinimumsFields,
    
    getCurrent: (configuration) => {
        let pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request
        .query(selectCurrent);
    },

    postUpdate: (configuration) => {
        let pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request
        .query(closePrevRecord);
    }

};