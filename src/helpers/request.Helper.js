
module.exports = {

    formatRequest: function(details) {
    var sizResArray = [];
    var minDefArray = [];
    var marDefArray = [];
    var iArray = [];
    var fsArray = [];
    var sizeRestrictionBackhaulItem, sizeRestrictionBalancedItem, sizeRestrictionHeadhaulItem, minimumDefaultsItem, marginDefaulBackhaulItem, marginDefaulBalancedItem, marginDefaulHeadhaulItem, intraterminallItem, fuelSurchargeItem;
    var response;

    sizeRestrictionBackhaulItem = {
        "update": (details.bkhaulwithinloh2 || details.bkhaulminweight2 || details.bkhaulmaxweight2 || details.bkhaulminlength2 || details.bkhaulmaxlength2 || details.bkhaulminunits2 || details.bkhaulmaxunits2) ? 1 : 0,
        "type": "backhaul",
        "loh": (details.bkhaulwithinloh2) ? details.bkhaulwithinloh2 : details.bkhaulwithinloh1,
        "minweight": (details.bkhaulminweight2) ? details.bkhaulminweight2 : details.bkhaulminweight1,
        "maxweight": (details.bkhaulmaxweight2) ? details.bkhaulmaxweight2 : details.bkhaulmaxweight1,
        "minlength": (details.bkhaulminlength2) ? details.bkhaulminlength2 : details.bkhaulminlength1,
        "maxlength": (details.bkhaulmaxlength2) ? details.bkhaulmaxlength2 : details.bkhaulmaxlength1,
        "minunits": (details.bkhaulminunits2) ? details.bkhaulminunits2 : details.bkhaulminunits1,
        "maxunits": (details.bkhaulmaxunits2) ? details.bkhaulmaxunits2 : details.bkhaulmaxunits1,
    };

    sizeRestrictionBalancedItem = {
        "update": (details.balwithinloh2 || details.balminweight2 || details.balmaxweight2 || details.balminlength2 || details.balmaxlength2 || details.balminunits2 || details.balmaxunits2) ? 1 : 0,
        "type": "balanced",
        "loh": (details.balwithinloh2) ? details.balwithinloh2 : details.balwithinloh1,
        "minweight": (details.balminweight2) ? details.balminweight2 : details.balminweight1,
        "maxweight": (details.balmaxweight2) ? details.balmaxweight2 : details.balmaxweight1,
        "minlength": (details.balminlength2) ? details.balminlength2 : details.balminlength1,
        "maxlength": (details.balmaxlength2) ? details.balmaxlength2 : details.balmaxlength1,
        "minunits": (details.balminunits2) ? details.balminunits2 : details.balminunits1,
        "maxunits": (details.balmaxunits2) ? details.balmaxunits2 : details.balmaxunits1,
    };

    sizeRestrictionHeadhaulItem = {
        "update": (details.headwithinloh2 || details.headminweight2 || details.headmaxweight2 || details.headminlength2 || details.headmaxlength2 || details.headminunits2 || details.headmaxunits2) ? 1 : 0,
        "type": "headhaul",
        "loh": (details.headwithinloh2) ? details.headwithinloh2 : details.headwithinloh1,
        "minweight": (details.headminweight2) ? details.headminweight2 : details.headminweight1,
        "maxweight": (details.headmaxweight2) ? details.headmaxweight2 : details.headmaxweight1,
        "minlength": (details.headminlength2) ? details.headminlength2 : details.headminlength1,
        "maxlength": (details.headmaxlength2) ? details.headmaxlength2 : details.headmaxlength1,
        "minunits": (details.headminunits2) ? details.headminunits2 : details.headminunits1,
        "maxunits": (details.headmaxunits2) ? details.headmaxunits2 : details.headmaxunits1,
    };

    minimumDefaultsItem = {
        "update": (details.defaultminimum2) ? 1 : 0,
        "minimum": (details.defaultminimum2) ? details.defaultminimum2 : details.defaultminimum1
    };

    marginDefaulBackhaulItem = {
        "update": (details.defaultbackhaul2) ? 1 : 0,
        "balance": "backhaul",
        "margin": (details.defaultbackhaul2) ? details.defaultbackhaul2 : details.defaultbackhaul1
    };

    marginDefaulBalancedItem = {
        "update": (details.defaultbalanced2) ? 1 : 0,
        "balance": "balanced",
        "margin": (details.defaultbalanced2) ? details.defaultbalanced2 : details.defaultbalanced1
    };

    marginDefaulHeadhaulItem = {
        "update": (details.defaultheadhaul2) ? 1 : 0,
        "balance": "headhaul",
        "margin": (details.defaultheadhaul2) ? details.defaultheadhaul2 : details.defaultheadhaul1
    };

    intraterminallItem = {
        "update": (details.intraterminalmargin2 || details.intraterminalminimum2 || details.intraterminaldiscount2 || details.intraterminalminweight2 || details.intraterminalmaxweight2 || details.intraterminalminlength2 || details.intraterminalmaxlength2) ? 1 : 0,
        "Effective_Date": 0,
        "Intraterminal_Margin": (details.intraterminalmargin2) ? details.intraterminalmargin2 : details.intraterminalmargin1,
        "Intraterminal_Minimum": (details.intraterminalminimum2) ? details.intraterminalminimum2 : details.intraterminalminimum1,
        "Intraterminal_Discount": (details.intraterminaldiscount2) ? details.intraterminaldiscount2 : details.intraterminaldiscount1,
        "Min_weight": (details.intraterminalminweight2) ? details.intraterminalminweight2 : details.intraterminalminweight1,
        "Max_weight": (details.intraterminalmaxweight2) ? details.intraterminalmaxweight2 : details.intraterminalmaxweight1,
        "Min_length": (details.intraterminalminlength2) ? details.intraterminalminlength2 : details.intraterminalminlength1,
        "Max_length": (details.intraterminalmaxlength2) ? details.intraterminalmaxlength2 : details.intraterminalmaxlength1
    };

    fuelSurchargeItem = {
        "update": (details.baserate2) ? 1 : 0,
        "percentage": (details.baserate2) ? details.baserate2 : details.baserate1,
        "type": "base"
    };

    sizResArray.push(sizeRestrictionBackhaulItem);
    sizResArray.push(sizeRestrictionBalancedItem);
    sizResArray.push(sizeRestrictionHeadhaulItem);
    minDefArray.push(minimumDefaultsItem);
    marDefArray.push(marginDefaulBackhaulItem);
    marDefArray.push(marginDefaulBalancedItem);
    marDefArray.push(marginDefaulHeadhaulItem);
    iArray.push(intraterminallItem);
    fsArray.push(fuelSurchargeItem);

    response = {
        "sizerestrictions": sizResArray,
        "minimumdefaults": minDefArray,
        "margindefault": marDefArray,
        "intraterminal": iArray,
        "fuelsurcharge": fsArray
    };
}
};