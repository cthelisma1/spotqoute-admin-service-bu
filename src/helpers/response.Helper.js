const errorsts = 'ERROR';
const oksts = 'OK';

var errList = {
    V0001: { code: 'V0001', msg: 'userid is required to authenticate' },
    V0002: { code: 'V0002', msg: 'userid must be with in 4 and 10 in length' },
    V0003: { code: 'V0003', msg: 'password is required to authenticate' },
    V0004: { code: 'V0004', msg: 'password must be with in 4 and 10 in length' },
    E9999: { code: 'E9999', msg: 'An unexpected error occurred during {0} operation' }
};

module.exports = {

    errors: errList,

    sendHello: function(res, logger) {
        res.status(200).json(buildResponse("OK", 'Microservice CRUD api. Database: msLegend, Table: services, Server: JCS-NAS:1433', logger));
    },

    sendSuccess: function(res, results, logger) {
        res.status(200).json(buildResponse(results, logger));
    },

    sendUnauthorized: function(res, logger) {
        res.status(401).json(buildResponse("Unauthorized", "Forbidden - Authenication header missing or invalid.", logger));
    },
    
    sendFailedValidation: function(res, details, logger) {
        res.status(400).json(buildErrorResponse(details.code, details.msg, logger));
    },

    sendFailure: function(res, identifier, error, logger) {
        let tmp = errList.E9999.msg.replace('{0}', identifier);
        res.status(500).json(buildErrorResponse(errList.E9999.code, `${tmp}\n\r ${error}`, logger));
    },
    buildModelResponse: function(values) {
        let response = {
            bkhaulwithinloh1: values[0].recordset[0].LOH,
            bkhaulminweight1: values[0].recordset[0].Min_weight,
            bkhaulmaxweight1: values[0].recordset[0].Max_weight,
            bkhaulminlength1: values[0].recordset[0].Min_length,
            bkhaulmaxlength1: values[0].recordset[0].Max_length,
            bkhaulminunits1: values[0].recordset[0].Min_units,
            bkhaulmaxunits1: values[0].recordset[0].Max_units,

            balwithinloh1: values[0].recordset[1].LOH,
            balminweight1: values[0].recordset[1].Min_weight,
            balmaxweight1: values[0].recordset[1].Max_weight,
            balminlength1: values[0].recordset[1].Min_length,
            balmaxlength1: values[0].recordset[1].Max_length,
            balminunits1: values[0].recordset[1].Min_units,
            balmaxunits1: values[0].recordset[1].Max_units,

            headwithinloh1: values[0].recordset[2].LOH,
            headminweight1: values[0].recordset[2].Min_weight,
            headmaxweight1: values[0].recordset[2].Max_weight,
            headminlength1: values[0].recordset[2].Min_length,
            headmaxlength1: values[0].recordset[2].Max_length,
            headminunits1: values[0].recordset[2].Min_units,
            headmaxunits1: values[0].recordset[2].Max_units,

            defaultminimum1: values[1].recordset[0].Minimum,
            defaultbackhaul1: values[2].recordset[0].Margin,
            defaultbalanced1: values[2].recordset[1].Margin,
            defaultheadhaul1: values[2].recordset[2].Margin,

            intraterminalmargin1: values[3].recordset[0].Intraterminal_Margin,
            intraterminalminimum1: values[3].recordset[0].Intraterminal_Minimum,
            intraterminaldiscount1: values[3].recordset[0].Intraterminal_Discount,
            intraterminalminweight1: values[3].recordset[0].Min_weight,
            intraterminalmaxweight1: values[3].recordset[0].Max_weight,
            intraterminalminlength1: values[3].recordset[0].Min_length,
            intraterminalmaxlength1: values[3].recordset[0].Max_length,
            baserate1: values[4].recordset[0].Percentage,
        };
        return response;
      },
    sendWorkbook: function(res, workbook, filename) {
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + filename);
    
        workbook.xlsx.write(res).then(function() {
          res.end();
        });
      }
};

function buildResponse(results, logger) {
    var response = {
        status: oksts,
        result: results
    }
    logger.stream.write(response);
    return response;
}

function buildErrorResponse(errorCode, errorMessage, logger) {
    var response = {
        status: errorsts,
        result: {
            ErrorCode: errorCode,
            Message: errorMessage
        }
    };

    logger.stream.write(response);
    return response;
}