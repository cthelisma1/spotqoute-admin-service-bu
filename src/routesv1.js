import Router from 'express';
import { check, validationResult } from 'express-validator/check';
import responses from './helpers/response.Helper';
import { version } from '../package.json';
import sqadminservice from './services/squoteadmin.service';

let configuration;

export default ({ db, msdb, logger }) => {
  // Microservice route setup for all actions.
  let api = Router();

  // configuration = {as400: db, msdb, logger};
  configuration = { db, msdb, logger };

  // Authentication
  api.get('/authentication', (req, res, next) => {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, sqadminservice.authentication);
  });


  // Details: LaneMinimumDefaults, LaneMarginDefaults, LaneSizeMaximums, LaneSizeMinimums, Intraterminal_settings table(s)
  api.get('/details/current', (req, res, next) => {
    // console.log(configuration.logger);
        // console.log("start");
    validate(req, res, next, configuration.logger, sqadminservice.currentDetails);
  });

  api.post('/details/update', check('body'), (req, res, next) => {
    // console.log(configuration.logger);
    // console.log("Update Details Route");
    validate(req, res, next, configuration.logger, sqadminservice.updateDetails);
  });


  // Lane Balance: LaneBalance table
  api.get('/laneBalance/current', (req, res, next) => {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, sqadminservice.currentLaneBalances);
  });

  api.post('/laneBalance/update',
    check('body'),
    (req, res, next) => {
      // console.log(configuration.logger);
      validate(req, res, next, configuration.logger, sqadminservice.updateLaneBalances);
    });


  // Lane Exclusions: LaneExclusions table
  api.get('/laneExclusions/current', (req, res, next) => {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, sqadminservice.currentLaneExclusions);
  });

  api.post('/laneExclusions/update',
    check('body')
    , (req, res, next) => {
      validate(req, res, next, configuration.logger, sqadminservice.updateLaneExclusions);
    });


  // Minimum Charge: ChargeLaneMinimumOverrides table
  api.get('/minimumCharge/current', (req, res, next) => {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, sqadminservice.currentMinimumCharge);
  });

  api.post('/minimumCharge/update',
    check('body')
    , (req, res, next) => {
      validate(req, res, next, configuration.logger, sqadminservice.updateMinimumCharge);
    });

    
  // Profits: LaneMarginOverrides table
  api.get('/profits/current', (req, res, next) => {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, sqadminservice.currentProfit);
  });

  api.post('/profits/update', check('body'), (req, res, next) => {
    validate(req, res, next, configuration.logger, sqadminservice.updateUpdate);
  });

  return api;
}

// Utility function to determine if validation errors occurred and report them are proceed with the action requested.
function validate(req, res, next, logger, func) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    responses.sendFailedValidation(res, errors.mapped(), logger);
  } else {
    func(req, res, configuration);
  }
}