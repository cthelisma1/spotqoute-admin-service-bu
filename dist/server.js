'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _https = require('https');

var _https2 = _interopRequireDefault(_https);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _winston = require('./config/winston');

var _winston2 = _interopRequireDefault(_winston);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _config = require('./config.json');

var _config2 = _interopRequireDefault(_config);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _expressValidator = require('express-validator');

var _expressValidator2 = _interopRequireDefault(_expressValidator);

var _swaggerUiExpress = require('swagger-ui-express');

var _swaggerUiExpress2 = _interopRequireDefault(_swaggerUiExpress);

var _swagger = require('./config/swagger.json');

var _swagger2 = _interopRequireDefault(_swagger);

var _as = require('./config/as400');

var _as2 = _interopRequireDefault(_as);

var _mssql = require('./config/mssql');

var _mssql2 = _interopRequireDefault(_mssql);

var _routesv = require('./routesv1');

var _routesv2 = _interopRequireDefault(_routesv);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _response = require('./helpers/response.Helper');

var _response2 = _interopRequireDefault(_response);

var _constants = require('constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Setup server level main objects
var app = (0, _express2.default)();
// import asData from './datalayers/squoteAS400.data';
// Setup Http end point for service DB request.

// Imports needed for Main module.


var logger = _winston2.default;

app.use((0, _cors2.default)());

// Setup system wide logging.
app.use((0, _morgan2.default)("combined", { "stream": _winston2.default.stream }));

// Setup http body parsing of data.
app.use(_bodyParser2.default.json({ limit: _config2.default.bodyLimit }));

// Setup url encoding/decoding
app.use(_bodyParser2.default.urlencoded({ extended: true }));

// Create instance/initialize validator object to use in application.
app.use((0, _expressValidator2.default)());

// Initialize the documentation infrastructure
app.use('/api-docs', _swaggerUiExpress2.default.serve, _swaggerUiExpress2.default.setup(_swagger2.default));

// Redirect all traffic to HTTPS secure protocol.
// app.use(function(req, res, next) {
//     if (req.secure) {
//         next();

//     } else {
//         res.redirect(`https://${req.host}:${require('./config.json').sslport}${req.url}`);
//     }
// });

// Authenticate the user knows the standard authentication key and passed it.
// app.use((req, res, next) => {
//     var nodeSSPI = require('node-sspi')
//     var nodeSSPIObj = new nodeSSPI({
//       retrieveGroups: true
//     })
//     nodeSSPIObj.authenticate(req, res, function(err){
//       res.finished || next()
//     })
//   });

// Open AS400 database connection pool.
(0, _as2.default)(function (db) {

    // Open MSSQL database connection pool.
    (0, _mssql2.default)(function (msdb) {
        // MSSQL connection failed. Report error abort start.
        if (msdb instanceof Error) {
            console.log(msdb);
            logger.log('error', msdb.message);
        } else {

            try {
                // Setup global routes - For release version 1.
                app.use('/v1', (0, _routesv2.default)({ db: db, msdb: msdb, logger: logger }));

                // Display web service documentation if browsing to web site root.
                app.get('/', function (req, res) {
                    res.redirect('/api-docs');
                });

                // catch 404
                app.all('*', function (req, res) {
                    // console.log("1");
                    _response2.default.sendFailure(res, "server_init", "Route not found", _winston2.default);
                });

                try {
                    // START THE SERVER
                    // =============================================================================
                    app.httpserver = _http2.default.createServer(app);

                    app.httpserver.listen(process.env.port || _config2.default.port, _config2.default.serveraddress, function () {
                        console.log('HTTP Server started at ' + app.httpserver.address().address + ' listing on port ' + app.httpserver.address().port);
                    });

                    var ssloptions = {
                        pfx: _fs2.default.readFileSync('./src/config/starsaia2019.pfx'),
                        passphrase: "Intimg1".trim(),
                        secureOptions: require('constants').SSL_OP_NO_TLSv1
                    };

                    app.httpsserver = _https2.default.createServer(ssloptions, app);

                    app.httpsserver.listen(process.env.port || _config2.default.sslport, _config2.default.serveraddress, function () {
                        console.log('HTTPS Server started at ' + app.httpsserver.address().address + ' listing on port ' + app.httpsserver.address().port);
                    });
                } catch (err) {
                    var msg = 'Server failed to start at HTTP server initialization ' + err;
                    console.log(msg);
                    logger.log({ level: 'error', message: msg });
                }
            } catch (err) {
                var _msg = 'Server failed to start at route loading and swagger documentation load with error ' + err;
                console.log(_msg);
                logger.log('error', _msg);
            }
        }
    });
});

exports.default = app;
//# sourceMappingURL=server.js.map