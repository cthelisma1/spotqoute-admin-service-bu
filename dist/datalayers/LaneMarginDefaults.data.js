'use strict';

var sql = require('mssql');

// All sql statements
var selectCurrent = 'SELECT * FROM PRICING_APP.dbo.LaneMarginDefaults WHERE EndDate IS NULL order by balance ';
var closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneMarginDefaults SET EndDate = GETDATE() WHERE Balance = @Balance and EndDate IS NULL';
var newRecord = 'INSERT INTO PRICING_APP.dbo.LaneMarginDefaults(Balance,Margin,StartDate,EndDate,ChangedBy) VALUES(@Balance,@Margin,GETDATE(),NULL,@ChangedBy)';
var LaneMarginDefaultsFields = {
    Balance: { name: 'Balance', dtype: sql.VarChar, length: 100 },
    Margin: { name: 'Margin', dtype: sql.Int, length: 4000 },
    StartDate: { name: 'StartDate', dtype: sql.DateTime, length: 100 },
    EndDate: { name: 'EndDate', dtype: sql.DateTime, length: 100 },
    ChangedBy: { name: 'ChangedBy', dtype: sql.VarChar, length: 4000 }
};
// backhaul, balanced, headhaul
module.exports = {

    // List of update table fields in the table.
    LaneMarginDefaultsFieldsList: LaneMarginDefaultsFields,

    getCurrent: function getCurrent(configuration) {

        var pool = configuration.db.pool;
        var request = new sql.Request(pool);
        return request.query(selectCurrent);
    },

    postUpdate: function postUpdate(req, configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        console.log("Markup/Discount: Discounts");
        console.log(req.body.margindefault[0].update == 1 ? "Update Backhaul" : "No Backhaul update");
        if (req.body.margindefault[0].update == 1) {
            var _pool = configuration.db.pool;
            request = new sql.Request(_pool);
            request.input(LaneMarginDefaultsFields.Balance.name, LaneMarginDefaultsFields.Balance.dtype, req.body.margindefault[0].balance).query(closePrevRecord);
            console.log("  Previous Lane Margin Default Closed: Backhaul");

            request.input(LaneMarginDefaultsFields.Balance.name, LaneMarginDefaultsFields.Balance.dtype, req.body.margindefault[0].balance).input(LaneMarginDefaultsFields.Margin.name, LaneMarginDefaultsFields.Margin.dtype, req.body.margindefault[0].margin).input(LaneMarginDefaultsFields.ChangedBy.name, LaneMarginDefaultsFields.ChangedBy.dtype, "chris").query(newRecord);
            console.log("  New Lane Margin Default Closed, Backhaul: " + req.body.margindefault[0].margin);
        }
        console.log(req.body.margindefault[1].update == 1 ? "Update Balanced" : "No Balanced update");
        if (req.body.margindefault[1].update == 1) {
            var _pool2 = configuration.db.pool;
            request = new sql.Request(_pool2);
            request.input(LaneMarginDefaultsFields.Balance.name, LaneMarginDefaultsFields.Balance.dtype, req.body.margindefault[1].balance).query(closePrevRecord);
            console.log("  Previous Lane Margin Default Closed: Balanced");

            request.input(LaneMarginDefaultsFields.Balance.name, LaneMarginDefaultsFields.Balance.dtype, req.body.margindefault[1].balance).input(LaneMarginDefaultsFields.Margin.name, LaneMarginDefaultsFields.Margin.dtype, req.body.margindefault[1].margin).input(LaneMarginDefaultsFields.ChangedBy.name, LaneMarginDefaultsFields.ChangedBy.dtype, "chris").query(newRecord);
            console.log("  New Lane Margin Default Closed, Balanced: " + req.body.margindefault[1].margin);
        }
        console.log(req.body.margindefault[2].update == 1 ? "Update Headhaul" : "No Headhaul update");
        if (req.body.margindefault[2].update == 1) {
            var _pool3 = configuration.db.pool;
            request = new sql.Request(_pool3);
            request.input(LaneMarginDefaultsFields.Balance.name, LaneMarginDefaultsFields.Balance.dtype, req.body.margindefault[2].balance).query(closePrevRecord);
            console.log("  Previous Lane Margin Default Closed: Headhaul");
            request.input(LaneMarginDefaultsFields.Balance.name, LaneMarginDefaultsFields.Balance.dtype, req.body.margindefault[2].balance).input(LaneMarginDefaultsFields.Margin.name, LaneMarginDefaultsFields.Margin.dtype, req.body.margindefault[2].margin).input(LaneMarginDefaultsFields.ChangedBy.name, LaneMarginDefaultsFields.ChangedBy.dtype, "chris").query(newRecord);
            console.log("  New Lane Margin Default Closed, Headhaul: " + req.body.margindefault[2].margin);
        }
        console.log("---------------------------------------------------------");
        return request.query(selectCurrent);
    }

};
//# sourceMappingURL=LaneMarginDefaults.data.js.map