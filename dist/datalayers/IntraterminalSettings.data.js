'use strict';

var sql = require('mssql');

// All sql statements
var selectCurrent = 'SELECT * FROM PRICING_APP.dbo.Intraterminal_settings WHERE EndDate IS NULL';
var newRecord = 'INSERT INTO PRICING_APP.dbo.Intraterminal_settings(Effective_Date,Intraterminal_Margin,Intraterminal_Minimum,Intraterminal_Discount,Min_weight,Max_weight,Min_length,Max_length,StartDate,EndDate,ChangedBy) VALUES(@EffectiveDate,@Intraterminal_Margin,@Intraterminal_Minimum,@Intraterminal_Discount,@Min_weight,@Max_weight,@Min_length,@Max_length,GETDATE(),NULL,@ChangedBy)';
var closePrevRecord = 'UPDATE PRICING_APP.dbo.Intraterminal_settings SET EndDate = GETDATE() WHERE EndDate IS NULL';
var IntraterminalSettingsFields = {
    EffectiveDate: { name: 'EffectiveDate', dtype: sql.VarChar, length: 100 },
    IntraterminalMargin: { name: 'Intraterminal_Margin', dtype: sql.Float, length: 4000 },
    IntraterminalMinimum: { name: 'Intraterminal_Minimum', dtype: sql.Float, length: 100 },
    IntraterminalDiscount: { name: 'Intraterminal_Discount', dtype: sql.Float, length: 100 },
    StartDate: { name: 'StartDate', dtype: sql.DateTime, length: 4000 },
    EndDate: { name: 'EndDate', dtype: sql.DateTime, length: 4000 },
    Maxweight: { name: 'Max_weight', dtype: sql.VarChar, length: 100 },
    Minweight: { name: 'Min_weight', dtype: sql.VarChar, length: 4000 },
    Maxlength: { name: 'Max_length', dtype: sql.VarChar, length: 100 },
    Minlength: { name: 'Min_length', dtype: sql.VarChar, length: 100 },
    ChangedBy: { name: 'ChangedBy', dtype: sql.VarChar, length: 4000 }
};

module.exports = {

    // List of update table fields in the table.
    IntraterminalSettingsFieldsList: IntraterminalSettingsFields,

    getCurrent: function getCurrent(configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request.query(selectCurrent);
    },

    postUpdate: function postUpdate(req, configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        console.log(req.body.intraterminal[0].update == 1 ? "Update Intraterminal" : "No Intraterminal update");
        if (req.body.intraterminal[0].update == 1) {

            request.query(closePrevRecord);
            console.log("  Previous Closed: Intraterminal");
            // // Pull all services in database.
            request.input(IntraterminalSettingsFields.EffectiveDate.name, IntraterminalSettingsFields.EffectiveDate.dtype, 0).input(IntraterminalSettingsFields.IntraterminalMargin.name, IntraterminalSettingsFields.IntraterminalMargin.dtype, req.body.intraterminal[0].Intraterminal_Margin).input(IntraterminalSettingsFields.IntraterminalMinimum.name, IntraterminalSettingsFields.IntraterminalMinimum.dtype, req.body.intraterminal[0].Intraterminal_Minimum).input(IntraterminalSettingsFields.IntraterminalDiscount.name, IntraterminalSettingsFields.IntraterminalDiscount.dtype, req.body.intraterminal[0].Intraterminal_Discount).input(IntraterminalSettingsFields.Minweight.name, IntraterminalSettingsFields.Minweight.dtype, req.body.intraterminal[0].Min_weight).input(IntraterminalSettingsFields.Maxweight.name, IntraterminalSettingsFields.Maxweight.dtype, req.body.intraterminal[0].Max_weight).input(IntraterminalSettingsFields.Minlength.name, IntraterminalSettingsFields.Minlength.dtype, req.body.intraterminal[0].Min_length).input(IntraterminalSettingsFields.Maxlength.name, IntraterminalSettingsFields.Maxlength.dtype, req.body.intraterminal[0].Max_length).input(IntraterminalSettingsFields.ChangedBy.name, IntraterminalSettingsFields.ChangedBy.dtype, "chrthe").query(newRecord);
            console.log("  New Margin: " + req.body.intraterminal[0].Intraterminal_Margin);
            console.log("  New Minimum: " + req.body.intraterminal[0].Intraterminal_Minimum);
            console.log("  New Discount: " + req.body.intraterminal[0].Intraterminal_Discount);
            console.log("  New Min Weight: " + req.body.intraterminal[0].Min_weight);
            console.log("  New Max Weight: " + req.body.intraterminal[0].Max_weight);
            console.log("  New Min Length: " + req.body.intraterminal[0].Min_length);
            console.log("  New Max Length: " + req.body.intraterminal[0].Max_length);
            console.log("---------------------------------------------------------");
        }

        return request.query(selectCurrent);
    }

};
//# sourceMappingURL=IntraterminalSettings.data.js.map