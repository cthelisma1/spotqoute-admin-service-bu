'use strict';

var sql = require('mssql');
var xlsx = require('xlsx');

// All sql statements
var selectAllCurrent = 'SELECT OriginTerminal, DestinationTerminal, LoadPlan, Balance FROM PRICING_APP.dbo.LaneBalance WHERE EndDate IS NULL ORDER BY OriginTerminal, DestinationTerminal';
var closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneBalance SET EndDate = GETDATE(), ChangedBy = @ChangedBy WHERE Lane = @Lane and EndDate IS NULL';
var newRecord = 'INSERT INTO PRICING_APP.dbo.LaneBalance(OriginTerminal,DestinationTerminal,LoadPlan,Balance,StartDate,EndDate,Lane,ChangedBy) VALUES(@OriginTerminal,@DestinationTerminal,@LoadPlan,@Balance,GETDATE(),NULL,@Lane,@ChangedBy)';

var LaneBalanceFields = {
  OriginTerminal: { name: 'OriginTerminal', dtype: sql.NVarChar, length: 100 },
  DestinationTerminal: { name: 'DestinationTerminal', dtype: sql.NVarChar, length: 4000 },
  LoadPlan: { name: 'LoadPlan', dtype: sql.NVarChar, length: 1000 },
  Balance: { name: 'Balance', dtype: sql.Float, length: 1000 },
  StartDate: { name: 'StartDate', dtype: sql.DateTime, length: 100 },
  EndDate: { name: 'EndDate', dtype: sql.DateTime, length: 100 },
  Lane: { name: 'Lane', dtype: sql.VarChar, length: 100 },
  ChangedBy: { name: 'ChangedBy', dtype: sql.VarChar, length: 4000 }
};

module.exports = {

  // List of update table fields in the table.
  LaneBalanceFieldsList: LaneBalanceFields,

  getCurrent: function getCurrent(configuration) {
    var pool = configuration.db.pool;
    var request = new sql.Request(pool);

    // Pull all services in database.
    return request.query(selectAllCurrent);
  },

  postUpdate: function postUpdate(configuration) {
    var pool = configuration.db.pool;
    var request = new sql.Request(pool);
    var currentRecord;
    var fileMin;
    var currentDatabaseData = [];
    var workbook = xlsx.readFile('Lane Balance Data.xlsx');
    var snl = workbook.SheetNames;

    // convert Lane Balance Data.xlsx sheet to JSON - contains new values that should update LaneBalance.
    var newFileUpload = JSON.parse(JSON.stringify(xlsx.utils.sheet_to_json(workbook.Sheets[snl[0]])));

    // query LaneBalance
    request.query(selectAllCurrent, function (err, rows) {
      if (err) {
        throw err;
      } else {
        // Process data 
        setValue(rows);
      }
    });

    // get current values in LaneBalance and convert into JSON - and Update table accordingly. 
    function setValue(value) {
      currentDatabaseData = JSON.parse(JSON.stringify(value));

      var _loop = function _loop(i) {

        // find the correspondingly lane and return
        currentRecord = currentDatabaseData.recordset.filter(function (value) {
          return value.OriginTerminal == newFileUpload[i].OriginTerminal && value.DestinationTerminal == newFileUpload[i].DestinationTerminal;
        });

        // close out previous record by setting the EndDate with date.
        if (currentRecord[0].Balance != newFileUpload[i].Balance && currentRecord[0].length !== 0) {
          console.log("Current Balance: " + currentRecord.Balance + "New Balance: " + newFileUpload[i].Balance);
          if (currentRecord.length !== 0) {
            request.input(LaneBalanceFields.OriginTerminal.name, LaneBalanceFields.OriginTerminal.dtype, newFileUpload[i].OriginTerminal).input(LaneBalanceFields.DestinationTerminal.name, LaneBalanceFields.DestinationTerminal.dtype, newFileUpload[i].DestinationTerminal).query(closePrevRecord, function (err) {
              if (err) {
                throw err;
              }
            });
            console.log("Closed Record: OT=" + newFileUpload[i].OriginTerminal + " DT=" + newFileUpload[i].DestinationTerminal + " Old Balance:" + currentRecord[i].Balance);
          }

          // insert new record with proper values and a EndDate = null
          lane = newFileUpload[i].OriginTerminal + "-" + newFileUpload[i].DestinationTerminal;

          request.input(LaneBalanceFields.OriginTerminal.name, LaneBalanceFields.OriginTerminal.dtype, newFileUpload[i].OriginTerminal).input(LaneBalanceFields.DestinationTerminal.name, LaneBalanceFields.DestinationTerminal.dtype, newFileUpload[i].DestinationTerminal).input(LaneBalanceFields.LoadPlan.name, LaneBalanceFields.LoadPlan.dtype, currentRecord[i].LoadPlan).input(LaneBalanceFields.Balance.name, LaneBalanceFields.Balance.dtype, currentRecord[i].Balance).input(LaneBalanceFields.Lane.name, LaneBalanceFields.Lane.dtype, lane).input(LaneBalanceFields.ChangedBy.name, LaneBalanceFields.ChangedBy.dtype, "chris").query(newRecord, function (err) {
            if (err) {
              throw err;
            } else {
              console.log("New Record: OT=" + newFileUpload[i].OriginTerminal + " DT=" + newFileUpload[i].DestinationTerminal + " New Balance:" + newFileUpload[i].Balance);
            }
          });
          // }
        }
      };

      for (var i = 0; i < newFileUpload.length; i++) {
        var lane;

        _loop(i);
      }
    }
    return request.query(selectAllCurrent);
  }
};
//# sourceMappingURL=LaneBalance.data.js.map