'use strict';

var sql = require('mssql');

// All sql statements
var selectCurrent = 'SELECT * FROM PRICING_APP.dbo.SizeRestrictions WHERE EndDate IS NULL order by LaneBalance';
var closePrevRecord = 'UPDATE PRICING_APP.dbo.SizeRestrictions SET EndDate = GETDATE() WHERE LaneBalance = @LaneBalance and EndDate IS NULL';
var newRecord = 'INSERT INTO PRICING_APP.dbo.SizeRestrictions(LaneBalance,Min_weight,Max_weight,Min_length,Max_length,Min_units,Max_units,LOH,StartDate,EndDate,ChangedBy) VALUES(@LaneBalance,@Min_weight,@Max_weight,@Min_length,@Max_length,@Min_units,@Max_units,@LOH,GETDATE(),NULL,@ChangedBy)';
var SizeRestrictionsFields = {
    LaneBalance: { name: 'LaneBalance', dtype: sql.NChar, length: 100 },
    Min_weight: { name: 'Min_weight', dtype: sql.Int, length: 1000 },
    Max_weight: { name: 'Max_weight', dtype: sql.Int, length: 1000 },
    Min_length: { name: 'Min_length', dtype: sql.Int, length: 100 },
    Max_length: { name: 'Max_length', dtype: sql.Int, length: 4000 },
    Min_units: { name: 'Min_units', dtype: sql.Int, length: 4000 },
    Max_units: { name: 'Max_units', dtype: sql.Int, length: 4000 },
    LOH: { name: 'LOH', dtype: sql.Int, length: 100 },
    StartDate: { name: 'StartDate', dtype: sql.DateTime, length: 100 },
    EndDate: { name: 'EndDate', dtype: sql.Datetime, length: 100 },
    ChangedBy: { name: 'ChangedBy', dtype: sql.VarChar, length: 4000 }
};

module.exports = {

    // List of update table fields in the table.
    SizeRestrictionsFieldsList: SizeRestrictionsFields,

    getCurrent: function getCurrent(configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request.query(selectCurrent);
    },

    postUpdate: function postUpdate(req, configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        console.log("Size Restrictions");
        console.log(req.body.sizerestrictions[0].update == 1 ? "Update Backhaul" : "No Backhaul update");
        if (req.body.sizerestrictions[0].update == 1) {
            var _pool = configuration.db.pool;
            request = new sql.Request(_pool);

            request.input(SizeRestrictionsFields.LaneBalance.name, SizeRestrictionsFields.LaneBalance.dtype, req.body.sizerestrictions[0].type).query(closePrevRecord);
            console.log("  Previous Closed: Backhaul");

            request.input(SizeRestrictionsFields.LaneBalance.name, SizeRestrictionsFields.LaneBalance.dtype, req.body.sizerestrictions[0].type).input(SizeRestrictionsFields.Min_weight.name, SizeRestrictionsFields.Min_weight.dtype, req.body.sizerestrictions[0].minweight).input(SizeRestrictionsFields.Max_weight.name, SizeRestrictionsFields.Max_weight.dtype, req.body.sizerestrictions[0].maxweight).input(SizeRestrictionsFields.Min_length.name, SizeRestrictionsFields.Min_length.dtype, req.body.sizerestrictions[0].minlength).input(SizeRestrictionsFields.Max_length.name, SizeRestrictionsFields.Max_length.dtype, req.body.sizerestrictions[0].maxlength).input(SizeRestrictionsFields.Min_units.name, SizeRestrictionsFields.Min_units.dtype, req.body.sizerestrictions[0].minunits).input(SizeRestrictionsFields.Max_units.name, SizeRestrictionsFields.Max_units.dtype, req.body.sizerestrictions[0].maxunits).input(SizeRestrictionsFields.LOH.name, SizeRestrictionsFields.LOH.dtype, req.body.sizerestrictions[0].loh).input(SizeRestrictionsFields.ChangedBy.name, SizeRestrictionsFields.ChangedBy.dtype, "chris").query(newRecord);
            console.log("  New Min Weight: " + req.body.sizerestrictions[0].minweight);
            console.log("  New Max Weight: " + req.body.sizerestrictions[0].maxweight);
            console.log("  New Min Length: " + req.body.sizerestrictions[0].minlength);
            console.log("  New Max Length: " + req.body.sizerestrictions[0].maxlength);
            console.log("  New Min Units: " + req.body.sizerestrictions[0].minunits);
            console.log("  New Max Units: " + req.body.sizerestrictions[0].maxunits);
            console.log("  New LOH: " + req.body.sizerestrictions[0].loh);
        }
        console.log(req.body.sizerestrictions[1].update == 1 ? "Update Balanced" : "No Balanced update");
        if (req.body.sizerestrictions[1].update == 1) {
            var _pool2 = configuration.db.pool;
            request = new sql.Request(_pool2);
            request.input(SizeRestrictionsFields.LaneBalance.name, SizeRestrictionsFields.LaneBalance.dtype, req.body.sizerestrictions[1].type).query(closePrevRecord);
            console.log("  Previous Closed: Balanced");

            request.input(SizeRestrictionsFields.LaneBalance.name, SizeRestrictionsFields.LaneBalance.dtype, req.body.sizerestrictions[1].type).input(SizeRestrictionsFields.Min_weight.name, SizeRestrictionsFields.Min_weight.dtype, req.body.sizerestrictions[1].minweight).input(SizeRestrictionsFields.Max_weight.name, SizeRestrictionsFields.Max_weight.dtype, req.body.sizerestrictions[1].maxweight).input(SizeRestrictionsFields.Min_length.name, SizeRestrictionsFields.Min_length.dtype, req.body.sizerestrictions[1].minlength).input(SizeRestrictionsFields.Max_length.name, SizeRestrictionsFields.Max_length.dtype, req.body.sizerestrictions[1].maxlength).input(SizeRestrictionsFields.Min_units.name, SizeRestrictionsFields.Min_units.dtype, req.body.sizerestrictions[1].minunits).input(SizeRestrictionsFields.Max_units.name, SizeRestrictionsFields.Max_units.dtype, req.body.sizerestrictions[1].maxunits).input(SizeRestrictionsFields.LOH.name, SizeRestrictionsFields.LOH.dtype, req.body.sizerestrictions[1].loh).input(SizeRestrictionsFields.ChangedBy.name, SizeRestrictionsFields.ChangedBy.dtype, "chris").query(newRecord);
            console.log("  New Min Weight: " + req.body.sizerestrictions[1].minweight);
            console.log("  New Max Weight: " + req.body.sizerestrictions[1].maxweight);
            console.log("  New Min Length: " + req.body.sizerestrictions[1].minlength);
            console.log("  New Max Length: " + req.body.sizerestrictions[1].maxlength);
            console.log("  New Min Units: " + req.body.sizerestrictions[1].minunits);
            console.log("  New Max Units: " + req.body.sizerestrictions[1].maxunits);
            console.log("  New LOH: " + req.body.sizerestrictions[1].loh);
        }
        console.log(req.body.sizerestrictions[2].update == 1 ? "Update Headhaul" : "No Headhaul update");
        if (req.body.sizerestrictions[2].update == 1) {
            var _pool3 = configuration.db.pool;
            request = new sql.Request(_pool3);

            request.input(SizeRestrictionsFields.LaneBalance.name, SizeRestrictionsFields.LaneBalance.dtype, req.body.sizerestrictions[2].type).query(closePrevRecord);
            console.log("  Previous Closed: Headhaul");

            request.input(SizeRestrictionsFields.LaneBalance.name, SizeRestrictionsFields.LaneBalance.dtype, req.body.sizerestrictions[2].type).input(SizeRestrictionsFields.Min_weight.name, SizeRestrictionsFields.Min_weight.dtype, req.body.sizerestrictions[2].minweight).input(SizeRestrictionsFields.Max_weight.name, SizeRestrictionsFields.Max_weight.dtype, req.body.sizerestrictions[2].maxweight).input(SizeRestrictionsFields.Min_length.name, SizeRestrictionsFields.Min_length.dtype, req.body.sizerestrictions[2].minlength).input(SizeRestrictionsFields.Max_length.name, SizeRestrictionsFields.Max_length.dtype, req.body.sizerestrictions[2].maxlength).input(SizeRestrictionsFields.Min_units.name, SizeRestrictionsFields.Min_units.dtype, req.body.sizerestrictions[2].minunits).input(SizeRestrictionsFields.Max_units.name, SizeRestrictionsFields.Max_units.dtype, req.body.sizerestrictions[2].maxunits).input(SizeRestrictionsFields.LOH.name, SizeRestrictionsFields.LOH.dtype, req.body.sizerestrictions[2].loh).input(SizeRestrictionsFields.ChangedBy.name, SizeRestrictionsFields.ChangedBy.dtype, "chris").query(newRecord);
            console.log("  New Min Weight: " + req.body.sizerestrictions[2].minweight);
            console.log("  New Max Weight: " + req.body.sizerestrictions[2].maxweight);
            console.log("  New Min Length: " + req.body.sizerestrictions[2].minlength);
            console.log("  New Max Length: " + req.body.sizerestrictions[2].maxlength);
            console.log("  New Min Units: " + req.body.sizerestrictions[2].minunits);
            console.log("  New Max Units: " + req.body.sizerestrictions[2].maxunits);
            console.log("  New LOH: " + req.body.sizerestrictions[2].loh);
        }
        console.log("---------------------------------------------------------");
        return request.query(selectCurrent);
    }

};
//# sourceMappingURL=SizeRestrictions.data.js.map