'use strict';

var sql = require('mssql');

// All sql statements
var check = 'SELECT * FROM PRICING_APP.dbo.AdminPanelUser where username = @username';
var AdminPanelUserFields = {
    username: { name: 'username', dtype: sql.VarChar, length: 100 }
};

module.exports = {

    AdminPanelUserFieldList: AdminPanelUserFields,

    getCurrent: function getCurrent(req, configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);
        return request.input(AdminPanelUserFields.username.name, AdminPanelUserFields.username.dtype, req).query(check);
    }

};
//# sourceMappingURL=authentication.data.js.map