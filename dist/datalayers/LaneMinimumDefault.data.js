'use strict';

var sql = require('mssql');

// All sql statements
var getAllLaneMinimumDefaults = 'SELECT * FROM PRICING_APP.dbo.LaneMinimumDefaults';
var getLatest = 'SELECT * FROM PRICING_APP.dbo.LaneMinimumDefaults WHERE ENDATE IS NULL';
var LaneMinimumDefaultsFields = {
    Minimum: { name: 'Minimum', dtype: sql.NChar, length: 100 },
    StartDate: { name: 'StartDate', dtype: sql.NChar, length: 4000 },
    EndDate: { name: 'EndDate', dtype: sql.NChar, length: 1000 },
    ChangedBy: { name: 'ChangedBy', dtype: sql.NChar, length: 4000 }
};

module.exports = {

    // List of updateable fields in the table.
    LaneMinimumDefaultsFieldsList: LaneMinimumDefaultsFields,

    getAll: function getAll(configuration) {
        var pool = configuration.db.pool;

        var request = new sql.Request(pool);

        // Pull all services in database.
        return request.query(getAllLaneMinimumDefaults);
    }
};
//# sourceMappingURL=LaneMinimumDefault.data.js.map