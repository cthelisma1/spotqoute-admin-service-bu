'use strict';

var _LaneSizeMinimums = require('../../dist/datalayers/LaneSizeMinimums.data');

var sql = require('mssql');

// All sql statements
var selectCurrent = 'SELECT * FROM PRICING_APP.dbo.LaneMinimumDefaults WHERE EndDate IS NULL';
var closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneMinimumDefaults SET EndDate = GETDATE() WHERE EndDate IS NULL';
var newRecord = 'INSERT INTO PRICING_APP.dbo.LaneMinimumDefaults(Minimum,StartDate,EndDate,ChangedBy) VALUES(@Minimum,GETDATE(),NULL,@ChangedBy)';
var LaneMinimumDefaultsFields = {
    Minimum: { name: 'Minimum', dtype: sql.Int, length: 100 },
    StartDate: { name: 'StartDate', dtype: sql.DateTime, length: 4000 },
    EndDate: { name: 'EndDate', dtype: sql.DateTime, length: 1000 },
    ChangedBy: { name: 'ChangedBy', dtype: sql.VarChar, length: 4000 }
};

module.exports = {

    // List of update table fields in the table.
    LaneMinimumDefaultsFieldsList: LaneMinimumDefaultsFields,

    getCurrent: function getCurrent(configuration) {

        var pool = configuration.db.pool;
        var request = new sql.Request(pool);
        return request.query(selectCurrent);
    },

    postUpdate: function postUpdate(req, configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);
        console.log("Markup/Discount: Minimum");
        if (req.body.minimumdefaults[0].update == 1) {
            console.log("Update Minimum Default: True");
            request.query(closePrevRecord);
            console.log("Previous Lane Minimum: Closed");

            request.input(LaneMinimumDefaultsFields.Minimum.name, LaneMinimumDefaultsFields.Minimum.dtype, req.body.minimumdefaults[0].minimum).input(LaneMinimumDefaultsFields.ChangedBy.name, LaneMinimumDefaultsFields.ChangedBy.dtype, "chris").query(newRecord);

            console.log("New Lane Minimum: " + req.body.minimumdefaults[0].minimum);
        }
        console.log("---------------------------------------------------------");
        return request.query(selectCurrent);
    }

};
//# sourceMappingURL=LaneMinimumDefaults.data.js.map