'use strict';

var _response = require('../helpers/response.Helper');

var _response2 = _interopRequireDefault(_response);

var _IntraterminalSettings = require('../datalayers/IntraterminalSettings.data');

var _IntraterminalSettings2 = _interopRequireDefault(_IntraterminalSettings);

var _LaneBalance = require('../datalayers/LaneBalance.data');

var _LaneBalance2 = _interopRequireDefault(_LaneBalance);

var _LaneExclusions = require('../datalayers/LaneExclusions.data');

var _LaneExclusions2 = _interopRequireDefault(_LaneExclusions);

var _LaneMarginDefaults = require('../datalayers/LaneMarginDefaults.data');

var _LaneMarginDefaults2 = _interopRequireDefault(_LaneMarginDefaults);

var _LaneMarginOverrides = require('../datalayers/LaneMarginOverrides.data');

var _LaneMarginOverrides2 = _interopRequireDefault(_LaneMarginOverrides);

var _LaneMinimumDefaults = require('../datalayers/LaneMinimumDefaults.data');

var _LaneMinimumDefaults2 = _interopRequireDefault(_LaneMinimumDefaults);

var _LaneMinimumOverrides = require('../datalayers/LaneMinimumOverrides.data');

var _LaneMinimumOverrides2 = _interopRequireDefault(_LaneMinimumOverrides);

var _LaneMinimums = require('../datalayers/LaneMinimums.data');

var _LaneMinimums2 = _interopRequireDefault(_LaneMinimums);

var _LaneSizeMaximums = require('../datalayers/LaneSizeMaximums.data');

var _LaneSizeMaximums2 = _interopRequireDefault(_LaneSizeMaximums);

var _LaneSizeMinimums = require('../datalayers/LaneSizeMinimums.data');

var _LaneSizeMinimums2 = _interopRequireDefault(_LaneSizeMinimums);

var _FuelSurcharge = require('../datalayers/FuelSurcharge.data');

var _FuelSurcharge2 = _interopRequireDefault(_FuelSurcharge);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {

    currentDetails: function currentDetails(req, res, configuration) {
        _LaneMinimumDefaults2.default.getCurrent(configuration).then(function (result) {
            _response2.default.sendSuccess(res, result.recordset, configuration.logger);

            _LaneMinimumDefaults2.default.getCurrent(configuration).then(function (result) {
                _response2.default.sendSuccess(res, result.recordset, configuration.logger);

                _LaneMinimumDefaults2.default.getCurrent(configuration).then(function (result) {
                    _response2.default.sendSuccess(res, result.recordset, configuration.logger);

                    _LaneMinimumDefaults2.default.getCurrent(configuration).then(function (result) {
                        _response2.default.sendSuccess(res, result.recordset, configuration.logger);

                        _LaneMinimumDefaults2.default.getCurrent(configuration).then(function (result) {
                            _response2.default.sendSuccess(res, result.recordset, configuration.logger);

                            _LaneMinimumDefaults2.default.getCurrent(configuration).then(function (result) {
                                _response2.default.sendSuccess(res, result.recordset, configuration.logger);
                            }, function (error) {
                                _response2.default.sendFailure(res, 'servicesGetAll', error, configuration.logger);
                            }).catch(function (error) {
                                _response2.default.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger);
                            });
                        }, function (error) {
                            _response2.default.sendFailure(res, 'servicesGetAll', error, configuration.logger);
                        }).catch(function (error) {
                            _response2.default.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger);
                        });
                    }, function (error) {
                        _response2.default.sendFailure(res, 'servicesGetAll', error, configuration.logger);
                    }).catch(function (error) {
                        _response2.default.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger);
                    });
                }, function (error) {
                    _response2.default.sendFailure(res, 'servicesGetAll', error, configuration.logger);
                }).catch(function (error) {
                    _response2.default.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger);
                });
            }, function (error) {
                _response2.default.sendFailure(res, 'servicesGetAll', error, configuration.logger);
            }).catch(function (error) {
                _response2.default.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger);
            });
        }, function (error) {
            _response2.default.sendFailure(res, 'servicesGetAll', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger);
        });
    }
};
//# sourceMappingURL=seq.data.js.map