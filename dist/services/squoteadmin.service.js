'use strict';

var _response = require('../helpers/response.Helper');

var _response2 = _interopRequireDefault(_response);

var _excel = require('../helpers/excel.Helper');

var _excel2 = _interopRequireDefault(_excel);

var _authentication2 = require('../datalayers/authentication.data');

var _authentication3 = _interopRequireDefault(_authentication2);

var _FuelSurcharge = require('../datalayers/FuelSurcharge.data');

var _FuelSurcharge2 = _interopRequireDefault(_FuelSurcharge);

var _IntraterminalSettings = require('../datalayers/IntraterminalSettings.data');

var _IntraterminalSettings2 = _interopRequireDefault(_IntraterminalSettings);

var _LaneBalance = require('../datalayers/LaneBalance.data');

var _LaneBalance2 = _interopRequireDefault(_LaneBalance);

var _LaneExclusions = require('../datalayers/LaneExclusions.data');

var _LaneExclusions2 = _interopRequireDefault(_LaneExclusions);

var _LaneMarginDefaults = require('../datalayers/LaneMarginDefaults.data');

var _LaneMarginDefaults2 = _interopRequireDefault(_LaneMarginDefaults);

var _LaneMarginOverrides = require('../datalayers/LaneMarginOverrides.data');

var _LaneMarginOverrides2 = _interopRequireDefault(_LaneMarginOverrides);

var _LaneMinimumDefaults = require('../datalayers/LaneMinimumDefaults.data');

var _LaneMinimumDefaults2 = _interopRequireDefault(_LaneMinimumDefaults);

var _LaneMinimumOverrides = require('../datalayers/LaneMinimumOverrides.data');

var _LaneMinimumOverrides2 = _interopRequireDefault(_LaneMinimumOverrides);

var _SizeRestrictions = require('../datalayers/SizeRestrictions.data');

var _SizeRestrictions2 = _interopRequireDefault(_SizeRestrictions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {

    authentication: function authentication(req, res, configuration) {
        var windowsaccount = req.connection.user.replace(/SAIA\\/g, '');
        _authentication3.default.getCurrent(windowsaccount, configuration).then(function (result) {
            if (result.rowsAffected == 0) {
                console.log("DENIED");
            } else {
                console.log("PASSED");
                _response2.default.sendSuccess(res, result.recordset, configuration.logger);
            }
        }, function (error) {
            _response2.default.sendFailure(res, 'authentication', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'authenticationCatch', error, configuration.logger);
        });
    },

    currentDetails: function currentDetails(req, res, configuration) {
        // console.log("start");
        var promises = [];
        promises = [_SizeRestrictions2.default.getCurrent(configuration), _LaneMinimumDefaults2.default.getCurrent(configuration), _LaneMarginDefaults2.default.getCurrent(configuration), _IntraterminalSettings2.default.getCurrent(configuration), _FuelSurcharge2.default.getCurrent(configuration)];

        Promise.all(promises).then(function (values) {
            //  console.log(JSON.stringify(values));
            // console.log(values);
            var response = {
                bkhaulwithinloh1: values[0].recordset[0].LOH,
                bkhaulminweight1: values[0].recordset[0].Min_weight,
                bkhaulmaxweight1: values[0].recordset[0].Max_weight,
                bkhaulminlength1: values[0].recordset[0].Min_length,
                bkhaulmaxlength1: values[0].recordset[0].Max_length,
                bkhaulminunits1: values[0].recordset[0].Min_units,
                bkhaulmaxunits1: values[0].recordset[0].Max_units,

                balwithinloh1: values[0].recordset[1].LOH,
                balminweight1: values[0].recordset[1].Min_weight,
                balmaxweight1: values[0].recordset[1].Max_weight,
                balminlength1: values[0].recordset[1].Min_length,
                balmaxlength1: values[0].recordset[1].Max_length,
                balminunits1: values[0].recordset[1].Min_units,
                balmaxunits1: values[0].recordset[1].Max_units,

                headwithinloh1: values[0].recordset[2].LOH,
                headminweight1: values[0].recordset[2].Min_weight,
                headmaxweight1: values[0].recordset[2].Max_weight,
                headminlength1: values[0].recordset[2].Min_length,
                headmaxlength1: values[0].recordset[2].Max_length,
                headminunits1: values[0].recordset[2].Min_units,
                headmaxunits1: values[0].recordset[2].Max_units,

                defaultminimum1: values[1].recordset[0].Minimum,
                defaultbackhaul1: values[2].recordset[0].Margin,
                defaultbalanced1: values[2].recordset[1].Margin,
                defaultheadhaul1: values[2].recordset[2].Margin,

                intraterminalmargin1: values[3].recordset[0].Intraterminal_Margin,
                intraterminalminimum1: values[3].recordset[0].Intraterminal_Minimum,
                intraterminaldiscount1: values[3].recordset[0].Intraterminal_Discount,
                intraterminalminweight1: values[3].recordset[0].Min_weight,
                intraterminalmaxweight1: values[3].recordset[0].Max_weight,
                intraterminalminlength1: values[3].recordset[0].Min_length,
                intraterminalmaxlength1: values[3].recordset[0].Max_length,
                baserate1: values[4].recordset[0].Percentage
            };
            res.json(response);
        }, function (reject) {
            _response2.default.sendFailure(res, reject);
        });
    },

    updateDetails: function updateDetails(req, res, configuration) {
        // console.log(req.body.defaultminimum2);
        var promises = [];
        // console.log("Update detail service");
        promises = [_SizeRestrictions2.default.postUpdate(req, configuration), _LaneMinimumDefaults2.default.postUpdate(req, configuration), _LaneMarginDefaults2.default.postUpdate(req, configuration), _IntraterminalSettings2.default.postUpdate(req, configuration), _FuelSurcharge2.default.postUpdate(req, configuration)];

        Promise.all(promises).then(function (values) {
            //  console.log(values);
            console.log(JSON.stringify(values));
            var response = {
                bkhaulwithinloh2: values[0].recordset[0].LOH,
                bkhaulminweight2: values[0].recordset[0].Min_weight,
                bkhaulmaxweight2: values[0].recordset[0].Max_weight,
                bkhaulminlength2: values[0].recordset[0].Min_length,
                bkhaulmaxlength2: values[0].recordset[0].Max_length,
                bkhaulminunits2: values[0].recordset[0].Min_units,
                bkhaulmaxunits2: values[0].recordset[0].Max_units,

                balwithinloh2: values[0].recordset[1].LOH,
                balminweight2: values[0].recordset[1].Min_weight,
                balmaxweight2: values[0].recordset[1].Max_weight,
                balminlength2: values[0].recordset[1].Min_length,
                balmaxlength2: values[0].recordset[1].Max_length,
                balminunits2: values[0].recordset[1].Min_units,
                balmaxunits2: values[0].recordset[1].Max_units,

                headwithinloh2: values[0].recordset[2].LOH,
                headminweight2: values[0].recordset[2].Min_weight,
                headmaxweight2: values[0].recordset[2].Max_weight,
                headminlength2: values[0].recordset[2].Min_length,
                headmaxlength2: values[0].recordset[2].Max_length,
                headminunits2: values[0].recordset[2].Min_units,
                headmaxunits2: values[0].recordset[2].Max_units,

                defaultminimum2: values[1].recordset[0].Minimum,
                defaultbackhaul2: values[2].recordset[0].Margin,
                defaultbalanced2: values[2].recordset[1].Margin,
                defaultheadhaul2: values[2].recordset[2].Margin,

                intraterminalmargin2: values[3].recordset[0].Intraterminal_Margin,
                intraterminalminimum2: values[3].recordset[0].Intraterminal_Minimum,
                intraterminaldiscount2: values[3].recordset[0].Intraterminal_Discount,
                intraterminalminweight2: values[3].recordset[0].Min_weight,
                intraterminalmaxweight2: values[3].recordset[0].Max_weight,
                intraterminalminlength2: values[3].recordset[0].Min_length,
                intraterminalmaxlength2: values[3].recordset[0].Max_length,
                baserate2: values[4].recordset[0].Percentage
            };
            console.log(response);
            res.json(response);
        }, function (reject) {
            _response2.default.sendFailure(res, reject);
        });
    },

    currentLaneBalances: function currentLaneBalances(req, res, configuration) {
        _LaneBalance2.default.getCurrent(configuration).then(function (result) {

            var filename = 'Lane Balance Data.xlsx';
            // filename += 'Points by Terminals.xlsx';
            // console.log(result);
            _response2.default.sendWorkbook(res, _excel2.default.generateLaneBalanceExcelSheet(result), filename);

            // responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, function (error) {
            _response2.default.sendFailure(res, 'currentLaneBalances', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'currentLaneBalancesCatch', error, configuration.logger);
        });
    },

    updateLaneBalances: function updateLaneBalances(req, res, configuration) {
        _LaneBalance2.default.postUpdate(configuration).then(function (result) {
            _response2.default.sendSuccess(res, result.recordset, configuration.logger);
        }, function (error) {
            _response2.default.sendFailure(res, 'updateLaneBalances', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'updateLaneBalancesCatch', error, configuration.logger);
        });
    },

    currentLaneExclusions: function currentLaneExclusions(req, res, configuration) {
        _LaneExclusions2.default.getCurrent(configuration).then(function (result) {
            var filename = 'Lane Exclusion Data.xlsx';
            _response2.default.sendWorkbook(res, _excel2.default.generateLaneExclusionExcelSheet(result), filename);
            // responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, function (error) {
            _response2.default.sendFailure(res, 'currentLaneExclusions', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'currentLaneExclusionsCatch', error, configuration.logger);
        });
    },

    updateLaneExclusions: function updateLaneExclusions(req, res, configuration) {
        _LaneExclusions2.default.postUpdate(req, configuration).then(function (result) {
            _response2.default.sendSuccess(res, result.recordset, configuration.logger);
        }, function (error) {
            _response2.default.sendFailure(res, 'updateLaneExclusions', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'updateLaneExclusionsCatch', error, configuration.logger);
        });
    },

    currentMinimumCharge: function currentMinimumCharge(req, res, configuration) {
        _LaneMinimumOverrides2.default.getCurrent(configuration).then(function (result) {
            var filename = 'Minimum Charge Data.xlsx';
            // filename += 'Points by Terminals.xlsx';
            // console.log(result);
            _response2.default.sendWorkbook(res, _excel2.default.generateMinimumExcelSheet(result), filename);
            // responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, function (error) {
            _response2.default.sendFailure(res, 'currentMinimumCharge', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'currentMinimumChargeCatch', error, configuration.logger);
        });
    },

    updateMinimumCharge: function updateMinimumCharge(req, res, configuration) {
        // var windowsaccount = req.connection.user.replace(/SAIA\\/g,'');
        _LaneMinimumOverrides2.default.postUpdate(configuration).then(function (result) {
            _response2.default.sendSuccess(res, result.recordset, configuration.logger);
        }, function (error) {
            _response2.default.sendFailure(res, 'servicesGetAll', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'servicesGetAllCatch', error, configuration.logger);
        });
    },

    currentProfit: function currentProfit(req, res, configuration) {
        _LaneMarginOverrides2.default.getCurrent(configuration).then(function (result) {
            var filename = 'Profit Margin Data.xlsx';
            _response2.default.sendWorkbook(res, _excel2.default.generateProfitSheet(result), filename);
            // responseHelper.sendSuccess(res, result.recordset, configuration.logger);
        }, function (error) {
            _response2.default.sendFailure(res, 'currentProfit', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'currentProfitCatch', error, configuration.logger);
        });
    },

    updateUpdate: function updateUpdate(req, res, configuration) {
        _LaneMarginOverrides2.default.postUpdate(configuration).then(function (result) {
            _response2.default.sendSuccess(res, result.recordset, configuration.logger);
        }, function (error) {
            _response2.default.sendFailure(res, 'updateUpdate', error, configuration.logger);
        }).catch(function (error) {
            _response2.default.sendFailure(res, 'updateUpdateCatch', error, configuration.logger);
        });
    }

};
//# sourceMappingURL=squoteadmin.service.js.map