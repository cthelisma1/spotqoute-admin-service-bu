'use strict';

var sql = require('mssql');

// All sql statements
var selectCurrent = 'SELECT * FROM PRICING_APP.dbo.LaneSizeMinimums WHERE EndDate IS NULL';
var newRecord = 'INSERT INTO PRICING_APP.dbo.LaneSizeMinimums(OriginTerminal,DestinationTerminal,LoadPlan,MinimumWeight,MinimumLength,MinimumUnits,StartDate,EndDate,ChangedBy) VALUES(@OriginTerminal,@DestinationTerminal,@LoadPlan,@Minimum,@MinimumLength,@MinimumUnits,GETDATE(),NULL,@ChangedBy)';
var closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneSizeMinimums SET EndDate = GETDATE() WHERE EndDate IS NULL';
var LaneSizeMinimumsFields = {
    OriginTerminal: { name: 'OriginTerminal', dtype: sql.NChar, length: 4000 },
    DestinationTerminal: { name: 'DestinationTerminal', dtype: sql.NChar, length: 4000 },
    LoadPlan: { name: 'LoadPlan', dtype: sql.NChar, length: 4000 },
    MinimumWeight: { name: 'MinimumWeight', dtype: sql.NChar, length: 100 },
    MinimumLength: { name: 'MinimumLength', dtype: sql.NChar, length: 4000 },
    MinimumUnits: { name: 'MinimumUnits', dtype: sql.NChar, length: 1000 },
    StartDate: { name: 'StartDate', dtype: sql.NChar, length: 100 },
    EndDate: { name: 'EndDate', dtype: sql.NChar, length: 100 },
    ChangedBy: { name: 'ChangedBy', dtype: sql.NChar, length: 4000 }
};

module.exports = {

    // List of update table fields in the table.
    LaneSizeMinimumsFieldsList: LaneSizeMinimumsFields,

    getCurrent: function getCurrent(configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request.query(selectCurrent);
    },

    postUpdate: function postUpdate(configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request.query(closePrevRecord);
    }

};
//# sourceMappingURL=LaneSizeMinimums.data.js.map