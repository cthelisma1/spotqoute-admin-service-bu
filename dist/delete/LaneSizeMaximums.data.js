'use strict';

var sql = require('mssql');

// All sql statements
var selectCurrent = 'SELECT * FROM PRICING_APP.dbo.LaneSizeMaximums WHERE EndDate IS NULL';
var newRecord = 'INSERT INTO PRICING_APP.dbo.LaneSizeMaximums(WithinLOH,MaximumWeight,MaximumLength,StartDate,EndDate,ChangedBy) VALUES(@WithinLOH,@MaximumWeight,@MaximumLength,GETDATE(),NULL,@ChangedBy)';
var closePrevRecord = 'UPDATE PRICING_APP.dbo.LaneSizeMaximums SET EndDate = GETDATE() WHERE EndDate IS NULL';
var LaneSizeMaximumsFields = {
    WithinLOH: { name: 'WithinLOH', dtype: sql.NChar, length: 100 },
    MaximumWeight: { name: 'MaximumWeight', dtype: sql.NChar, length: 4000 },
    MaximumLength: { name: 'MaximumLength', dtype: sql.NChar, length: 4000 },
    StartDate: { name: 'StartDate', dtype: sql.NChar, length: 100 },
    EndDate: { name: 'EndDate', dtype: sql.NChar, length: 100 },
    ChangedBy: { name: 'ChangedBy', dtype: sql.NChar, length: 4000 }
};

module.exports = {

    // List of update table fields in the table.
    LaneSizeMaximumsFieldsList: LaneSizeMaximumsFields,

    getCurrent: function getCurrent(configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        // Pull all services in database.
        return request.query(selectCurrent);
    },

    postUpdate: function postUpdate(req, configuration) {
        var pool = configuration.db.pool;
        var request = new sql.Request(pool);

        request.query(closePrevRecord);

        // Pull all services in database.
        request.input(LaneSizeMaximumsFields.WithinLOH.name, LaneSizeMaximumsFields.WithinLOH.dtype, req.body.WithinLOH).input(LaneSizeMaximumsFields.MaximumWeight.name, LaneSizeMaximumsFields.MaximumWeight.dtype, req.body.MaximumWeight).input(LaneSizeMaximumsFields.MaximumLength.name, LaneSizeMaximumsFields.MaximumLength.dtype, req.body.MaximumLength).input(LaneSizeMaximumsFields.MaximumLength.name, LaneSizeMaximumsFields.MaximumLength.dtype, req.body.MaximumLength).query(newRecord);
        //console.log("Changed by: " + req.body.);
        return request.query(selectCurrent);
    }

};
//# sourceMappingURL=LaneSizeMaximums.data.js.map