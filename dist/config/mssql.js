'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

var _mssql = require('mssql');

var _mssql2 = _interopRequireDefault(_mssql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (callback) {
  try {
    _mssql2.default.connect(_config2.default.dbConfig.sqlConfig).then(function (pool) {
      callback(pool);
    }).catch(function (err) {
      callback(err);
    });
  } catch (err) {
    callback(err);
  }
};
//# sourceMappingURL=mssql.js.map