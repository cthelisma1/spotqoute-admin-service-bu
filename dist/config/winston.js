'use strict';

var winston = require('winston');
require('winston-daily-rotate-file');

var transport = new winston.transports.DailyRotateFile({
    "dirname": "logs",
    "filename": "service-%DATE%.log",
    "datePattern": "YYYY-MM-DD-HH",
    "zippedArchive": true,
    "maxSize": "10m",
    "maxFiles": "30d"
});

var logger = winston.createLogger({
    transports: [transport]
});

logger.stream = {
    write: function write(message) {
        logger.info(message);
    }
};

module.exports = logger;
//# sourceMappingURL=winston.js.map