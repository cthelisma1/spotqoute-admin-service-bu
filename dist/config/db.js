'use strict';

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sql = require('mssql');

module.exports = {

    init: function init(callback) {
        sql.connect(_config2.default.dbConfig.sqlConfig).then(function (pool) {
            callback({ pool: pool });
        }, function (err) {
            callback(err);
        });
    }
};
//# sourceMappingURL=db.js.map