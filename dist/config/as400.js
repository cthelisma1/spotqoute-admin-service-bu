'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

var _ibm_db = require('ibm_db');

var _ibm_db2 = _interopRequireDefault(_ibm_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (callback) {
  try {
    _ibm_db2.default.open(_config2.default.dbConfig.connectionString, function (err, pool) {
      if (err) {
        callback(err);
      } else {
        callback(pool);
      }
    });
  } catch (err) {
    callback(err);
  }
};
//# sourceMappingURL=as400.js.map