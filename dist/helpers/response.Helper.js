'use strict';

var errorsts = 'ERROR';
var oksts = 'OK';

var errList = {
    V0001: { code: 'V0001', msg: 'userid is required to authenticate' },
    V0002: { code: 'V0002', msg: 'userid must be with in 4 and 10 in length' },
    V0003: { code: 'V0003', msg: 'password is required to authenticate' },
    V0004: { code: 'V0004', msg: 'password must be with in 4 and 10 in length' },
    E9999: { code: 'E9999', msg: 'An unexpected error occurred during {0} operation' }
};

module.exports = {

    errors: errList,

    sendHello: function sendHello(res, logger) {
        res.status(200).json(buildResponse("OK", 'Microservice CRUD api. Database: msLegend, Table: services, Server: JCS-NAS:1433', logger));
    },

    sendSuccess: function sendSuccess(res, results, logger) {
        res.status(200).json(buildResponse(results, logger));
    },

    sendUnauthorized: function sendUnauthorized(res, logger) {
        res.status(401).json(buildResponse("Unauthorized", "Forbidden - Authenication header missing or invalid.", logger));
    },

    sendFailedValidation: function sendFailedValidation(res, details, logger) {
        res.status(400).json(buildErrorResponse(details.code, details.msg, logger));
    },

    sendFailure: function sendFailure(res, identifier, error, logger) {
        var tmp = errList.E9999.msg.replace('{0}', identifier);
        res.status(500).json(buildErrorResponse(errList.E9999.code, tmp + '\n\r ' + error, logger));
    },
    sendWorkbook: function sendWorkbook(res, workbook, filename) {
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + filename);

        workbook.xlsx.write(res).then(function () {
            res.end();
        });
    }
};

function buildResponse(results, logger) {
    var response = {
        status: oksts,
        result: results
    };
    logger.stream.write(response);
    return response;
}

function buildErrorResponse(errorCode, errorMessage, logger) {
    var response = {
        status: errorsts,
        result: {
            ErrorCode: errorCode,
            Message: errorMessage
        }
    };

    logger.stream.write(response);
    return response;
}
//# sourceMappingURL=response.Helper.js.map