'use strict';

var _exceljs = require('exceljs');

var _exceljs2 = _interopRequireDefault(_exceljs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
  generateLaneBalanceExcelSheet: function generateLaneBalanceExcelSheet(details) {
    console.log("Generate Lane Balance Excel Sheet");
    var workbook = new _exceljs2.default.Workbook();
    var worksheet = workbook.addWorksheet('Lane Balance');
    worksheet.columns = [{ header: 'Origin Terminal', key: 'originTerminal', width: 15 }, { header: 'Destination Terminal', key: 'destinationTerminal', width: 20 }, { header: 'Load Plan', key: 'loadPlan', width: 22 }, { header: 'Balance', key: 'balance', width: 8 }];

    worksheet.getRow(1).font = { bold: true };
    worksheet.getColumn(2).alignment = { horizontal: 'center' };
    worksheet.getColumn(3).alignment = { horizontal: 'center' };
    worksheet.getColumn(4).alignment = { horizontal: 'center' };
    // worksheet.getColumn(5).alignment = { horizontal: 'center' };
    // console.log(details);
    details.recordset.forEach(function (detail) {
      worksheet.addRow([detail.OriginTerminal, detail.DestinationTerminal, detail.LoadPlan, detail.Balance]);
    });

    return workbook;
  },
  generateLaneExclusionExcelSheet: function generateLaneExclusionExcelSheet(details) {
    console.log("Generate Lane Exclusion Excel Sheet");
    var workbook = new _exceljs2.default.Workbook();
    var worksheet = workbook.addWorksheet('Lane Exclusion');
    worksheet.columns = [{ header: 'Origin Terminal', key: 'originTerminal', width: 15 }, { header: 'Destination Terminal', key: 'destinationTerminal', width: 20 }, { header: 'Load Plan', key: 'loadPlan', width: 22 }, { header: 'Excluded', key: 'excluded', width: 8 }, { header: 'Reason', key: 'reason', width: 8 }];

    worksheet.getRow(1).font = { bold: true };
    worksheet.getColumn(2).alignment = { horizontal: 'center' };
    worksheet.getColumn(3).alignment = { horizontal: 'center' };
    worksheet.getColumn(4).alignment = { horizontal: 'center' };
    worksheet.getColumn(5).alignment = { horizontal: 'center' };

    details.recordset.forEach(function (detail) {
      worksheet.addRow([detail.OriginTerminal, detail.DestinationTerminal, detail.LoadPlan, detail.Excluded, detail.Reason]);
    });

    return workbook;
  },
  generateMinimumExcelSheet: function generateMinimumExcelSheet(details) {
    console.log("Generate Minimum Excel Sheet");
    var workbook = new _exceljs2.default.Workbook();
    var worksheet = workbook.addWorksheet('Minimum');
    worksheet.columns = [{ header: 'Origin Terminal', key: 'originTerminal', width: 15 }, { header: 'Destination Terminal', key: 'destinationTerminal', width: 20 }, { header: 'Minimum', key: 'minimum', width: 22 }];

    worksheet.getRow(1).font = { bold: true };
    worksheet.getColumn(2).alignment = { horizontal: 'center' };
    worksheet.getColumn(3).alignment = { horizontal: 'center' };

    details.recordset.forEach(function (detail) {
      worksheet.addRow([detail.OriginTerminal, detail.DestinationTerminal, detail.Minimum]);
    });

    return workbook;
  },
  generateProfitSheet: function generateProfitSheet(details) {
    console.log("Generate Profit Excel Sheet");
    var workbook = new _exceljs2.default.Workbook();
    var worksheet = workbook.addWorksheet('Profit Margin');
    worksheet.columns = [{ header: 'Origin Terminal', key: 'originTerminal', width: 15 }, { header: 'Destination Terminal', key: 'destinationTerminal', width: 20 }, { header: 'Lane', key: 'lane', width: 22 }, { header: 'Margin', key: 'margin', width: 22 }];

    worksheet.getRow(1).font = { bold: true };
    worksheet.getColumn(2).alignment = { horizontal: 'center' };
    worksheet.getColumn(3).alignment = { horizontal: 'center' };
    worksheet.getColumn(4).alignment = { horizontal: 'center' };

    details.recordset.forEach(function (detail) {
      worksheet.addRow([detail.OriginTerminal, detail.DestinationTerminal, detail.Lane, detail.Margin]);
    });

    return workbook;
  }
};
//# sourceMappingURL=excel.Helper.js.map