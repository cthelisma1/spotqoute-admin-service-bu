'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _check = require('express-validator/check');

var _response = require('./helpers/response.Helper');

var _response2 = _interopRequireDefault(_response);

var _package = require('../package.json');

var _squoteadmin = require('./services/squoteadmin.service');

var _squoteadmin2 = _interopRequireDefault(_squoteadmin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var configuration = void 0;

exports.default = function (_ref) {
  var db = _ref.db,
      msdb = _ref.msdb,
      logger = _ref.logger;

  // Microservice route setup for all actions.
  var api = (0, _express2.default)();

  // configuration = {as400: db, msdb, logger};
  configuration = { db: db, msdb: msdb, logger: logger };

  // Authentication
  api.get('/authentication', function (req, res, next) {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, _squoteadmin2.default.authentication);
  });

  // Details: LaneMinimumDefaults, LaneMarginDefaults, LaneSizeMaximums, LaneSizeMinimums, Intraterminal_settings table(s)
  api.get('/details/current', function (req, res, next) {
    // console.log(configuration.logger);
    // console.log("start");
    validate(req, res, next, configuration.logger, _squoteadmin2.default.currentDetails);
  });

  api.post('/details/update', (0, _check.check)('body'), function (req, res, next) {
    // console.log(configuration.logger);
    // console.log("Update Details Route");
    validate(req, res, next, configuration.logger, _squoteadmin2.default.updateDetails);
  });

  // Lane Balance: LaneBalance table
  api.get('/laneBalance/current', function (req, res, next) {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, _squoteadmin2.default.currentLaneBalances);
  });

  api.post('/laneBalance/update', (0, _check.check)('body'), function (req, res, next) {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, _squoteadmin2.default.updateLaneBalances);
  });

  // Lane Exclusions: LaneExclusions table
  api.get('/laneExclusions/current', function (req, res, next) {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, _squoteadmin2.default.currentLaneExclusions);
  });

  api.post('/laneExclusions/update', (0, _check.check)('body'), function (req, res, next) {
    validate(req, res, next, configuration.logger, _squoteadmin2.default.updateLaneExclusions);
  });

  // Minimum Charge: ChargeLaneMinimumOverrides table
  api.get('/minimumCharge/current', function (req, res, next) {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, _squoteadmin2.default.currentMinimumCharge);
  });

  api.post('/minimumCharge/update', (0, _check.check)('body'), function (req, res, next) {
    validate(req, res, next, configuration.logger, _squoteadmin2.default.updateMinimumCharge);
  });

  // Profits: LaneMarginOverrides table
  api.get('/profits/current', function (req, res, next) {
    // console.log(configuration.logger);
    validate(req, res, next, configuration.logger, _squoteadmin2.default.currentProfit);
  });

  api.post('/profits/update', (0, _check.check)('body'), function (req, res, next) {
    validate(req, res, next, configuration.logger, _squoteadmin2.default.updateUpdate);
  });

  return api;
};

// Utility function to determine if validation errors occurred and report them are proceed with the action requested.


function validate(req, res, next, logger, func) {
  var errors = (0, _check.validationResult)(req);
  if (!errors.isEmpty()) {
    _response2.default.sendFailedValidation(res, errors.mapped(), logger);
  } else {
    func(req, res, configuration);
  }
}
//# sourceMappingURL=routesv1.js.map